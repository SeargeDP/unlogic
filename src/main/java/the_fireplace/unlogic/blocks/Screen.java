package the_fireplace.unlogic.blocks;

import the_fireplace.unlogic.unbase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.util.ForgeDirection;

public class Screen extends Block{

	public Screen(Material p_i45394_1_) {
		super(p_i45394_1_);
	}
	@Override
    public boolean canDropFromExplosion(Explosion p_149659_1_)
    {
        return false;
    }
    @Override
    public int getLightOpacity(IBlockAccess world, int x, int y, int z)
    {
        return 1;
    }
    @Override
    public int getFlammability(IBlockAccess world, int x, int y, int z, ForgeDirection face)
    {
        return 240;
    }
}
