package the_fireplace.unlogic.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;

public class UnLogicGemBlock extends Block{

	public UnLogicGemBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockName("UnLogicGemBlock");
		setBlockTextureName("unlogic:gemBlock");
		setHardness(4);
	}
	@Override
	public boolean isBeaconBase(IBlockAccess worldObj, int x, int y, int z, int beaconX, int beaconY, int beaconZ)
    {
        return true;
    }
}
