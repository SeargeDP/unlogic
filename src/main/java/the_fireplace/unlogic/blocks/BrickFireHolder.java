package the_fireplace.unlogic.blocks;

import static net.minecraftforge.common.util.ForgeDirection.UP;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class BrickFireHolder extends Block{

	public BrickFireHolder(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockName("BrickFireHolder");
		setHardness(2);
	}
	//start texture code
	@SideOnly(Side.CLIENT)
	public static IIcon topIcon;
	@SideOnly(Side.CLIENT)
	public static IIcon bottomIcon;
	@SideOnly(Side.CLIENT)
	public static IIcon sideIcon;

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister icon) {
	topIcon = icon.registerIcon("unlogic:brickholder");
	bottomIcon = icon.registerIcon("minecraft:brick");
	sideIcon = icon.registerIcon("minecraft:brick");
	}
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata) {
	if(side == 0) {
	return bottomIcon;
	} else if(side == 1) {
	return topIcon;
	} else {
	return sideIcon;
	}
	}
	//End texture code
@Override
public boolean isFireSource(World world, int x, int y, int z, ForgeDirection side)
{
	if(side == UP){
		return true;
	}
	else
	{
		return false;
	}
}
}
