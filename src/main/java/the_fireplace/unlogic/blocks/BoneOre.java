package the_fireplace.unlogic.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.world.IBlockAccess;

public class BoneOre extends Block{

	public BoneOre(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockTextureName("unlogic:boneore");
        setHarvestLevel("pickaxe", 0);
        setBlockName("BoneOre");
        setHardness(5);
	}
@Override
public int getExpDrop(IBlockAccess world, int metadata, int fortune)
{
    return 2;
}
@Override
protected boolean canSilkHarvest()
{
    return true;
}
@Override
public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
{
    return Items.bone;
}
@Override
public int quantityDropped(Random p_149745_1_)
{
    return 2;
}
}
