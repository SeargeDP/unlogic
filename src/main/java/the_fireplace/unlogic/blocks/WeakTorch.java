package the_fireplace.unlogic.blocks;

import the_fireplace.unlogic.unbase;
import net.minecraft.block.BlockTorch;

public class WeakTorch extends BlockTorch{
	public WeakTorch(){
		setBlockName("BlockWeakTorch");
		setBlockTextureName("torch_on");
		setCreativeTab(unbase.TabUnLogic);
	}

}
