package the_fireplace.unlogic.armor;

import java.io.Console;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.config.UnLogicConfigValues;
import the_fireplace.fireplacecore.extendedclasses.ExtendedPlayer;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class FireArmor extends ItemArmor
{
	public FireArmor(ArmorMaterial p_i45325_1_, int p_i45325_2_,
			int p_i45325_3_) {
		super(p_i45325_1_, p_i45325_2_, p_i45325_3_);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		if(stack.getItem() == unbase.FireHelmet || stack.getItem() == unbase.FireChestplate || stack.getItem() == unbase.FireBoots) {
			return "unlogic:textures/models/armor/fire1.png";
		}
		if(stack.getItem() == unbase.FireLeggings) {
			return "unlogic:textures/models/armor/fire2.png";
		}
		return null;
	}
	private boolean print = false;
	private boolean ifArmor = false;
	private boolean ifHelmet = false;
	private boolean ifLeggings = false;
	private boolean ifAllArmor = false;
	private boolean nothelmet = false;
	private boolean notleggings = false;
	private boolean notallarmor = false;
	private boolean fireplace = false;
	@Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
    	if(UnLogicConfigValues.ENABLEDEBUG == true){
    	if(print == false){
    	System.out.println("[UnLogic] Armor Tick Update is functioning");
    	print ^= true;
    	}}
    	if (player.getCurrentArmor(0) != null && player.getCurrentArmor(1) != null && player.getCurrentArmor(2) != null && player.getCurrentArmor(3) != null) {

		ItemStack helmet = player.getCurrentArmor(3);
		ItemStack plate = player.getCurrentArmor(2);
		ItemStack legs = player.getCurrentArmor(1);
		ItemStack boots = player.getCurrentArmor(0); 
    	if (helmet.getItem() == unbase.FireHelmet && plate.getItem() == unbase.FireChestplate && legs.getItem() == unbase.FireLeggings && boots.getItem() == unbase.FireBoots) {
        	if(UnLogicConfigValues.ENABLEDEBUG == true){
			if(ifAllArmor == false){
		    	System.out.println("[UnLogic] Full set equipped");
		    	ifAllArmor  ^= true;
		    	}}
			player.addPotionEffect((new PotionEffect(Potion.fireResistance.getId(), 1, 0)));
		}}
			if(player.getCurrentArmor(3) !=null){
			ItemStack helmet = player.getCurrentArmor(3);
			
			if (helmet.getItem() == unbase.FireHelmet) {
	        	if(UnLogicConfigValues.ENABLEDEBUG == true){
				if(ifHelmet == false){
			    	System.out.println("[UnLogic] Headphones equipped");
			    	ifHelmet  ^= true;
			    	}}
				player.addPotionEffect((new PotionEffect(Potion.nightVision.getId(), 220, 0)));
			}}
			
			if(player.getCurrentArmor(1) != null){
				ItemStack legs = player.getCurrentArmor(1);
			if (legs.getItem() == unbase.FireLeggings) {
	        	if(UnLogicConfigValues.ENABLEDEBUG == true){
				if(ifLeggings == false){
			    	System.out.println("[UnLogic] Belt equipped");
			    	ifLeggings  ^= true;
			    	}}
				player.addPotionEffect((new PotionEffect(Potion.moveSpeed.getId(), 1, 0)));
			}}
			if (player.getCurrentArmor(0) != null && player.getCurrentArmor(1) != null && player.getCurrentArmor(2) != null && player.getCurrentArmor(3) != null) {

				ItemStack helmet = player.getCurrentArmor(3);
				ItemStack plate = player.getCurrentArmor(2);
				ItemStack legs = player.getCurrentArmor(1);
				ItemStack boots = player.getCurrentArmor(0); 
			if (player.getCommandSenderName().equals("The_Fireplace") || player.getUniqueID().toString() == "") {
				if (helmet.getItem() == unbase.FireHelmet && plate.getItem() == unbase.FireChestplate && legs.getItem() == unbase.FireLeggings && boots.getItem() == unbase.FireBoots) {
				if(fireplace == false){
			    	System.out.println("F1repl4ce Armor matched with The_Fireplace. The Minecraft universe is finally aligned. Prepare for epicness.");
			    	fireplace  ^= true;
			    	}
				player.addPotionEffect((new PotionEffect(Potion.digSpeed.getId(), 4000, 0)));
				player.addPotionEffect((new PotionEffect(Potion.damageBoost.getId(), 4000, 0)));
				player.addPotionEffect((new PotionEffect(Potion.jump.getId(), 4000, 0)));
				player.addPotionEffect((new PotionEffect(Potion.regeneration.getId(), 4000, 0)));
				player.addPotionEffect((new PotionEffect(Potion.resistance.getId(), 4000, 0)));
				player.addPotionEffect((new PotionEffect(Potion.waterBreathing.getId(), 4000, 0)));
				}}
			}
		}
}
    

