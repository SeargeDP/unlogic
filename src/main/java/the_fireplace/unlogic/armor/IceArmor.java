package the_fireplace.unlogic.armor;

import java.util.ArrayList;
import java.util.List;

import the_fireplace.unlogic.unbase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class IceArmor extends ItemArmor {

	public IceArmor(ArmorMaterial p_i45325_1_, int p_i45325_2_, int p_i45325_3_) {
		super(p_i45325_1_, p_i45325_2_, p_i45325_3_);
	}
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		if(stack.getItem() == unbase.IceHelmet || stack.getItem() == unbase.IceChestplate || stack.getItem() == unbase.IceBoots) {
			return "unlogic:textures/models/armor/ice1.png";
		}
		if(stack.getItem() == unbase.IceLeggings) {
			return "unlogic:textures/models/armor/ice2.png";
		}
		return null;
	}
	private static int tickCounter = 0;
	@Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
		if(!world.isRemote){
		ItemStack helmet = player.getCurrentArmor(3);
		ItemStack plate = player.getCurrentArmor(2);
		ItemStack legs = player.getCurrentArmor(1);
		ItemStack boots = player.getCurrentArmor(0);
		int a = 0; 
		int b = 0; 
		int c = 0; 
		int d = 0;
		if(helmet != null){
			if(helmet.getItem() == unbase.IceHelmet){
				a = 1;
			}else{
				a = 0;
			}
		}
		if(helmet == null){
			a = 0;
		}
		if(plate != null){
			if(plate.getItem() == unbase.IceChestplate){
				b = 1;
			}else{
				b = 0;
			}
		}
		if(plate == null){
			b = 0;
		}
		if(legs != null){
			if(legs.getItem() == unbase.IceLeggings){
				c = 1;
			}else{
				c = 0;
			}
		}
		if(legs == null){
			c = 0;
		}
		if(boots != null){
			if(boots.getItem() == unbase.IceBoots){
				d = 1;
			}else{
				d = 0;
			}
		}
		if(boots == null){
			d = 0;
		}
		//Safety checks
		if(a < 0){a = 0;}
		if(b < 0){b = 0;}
		if(c < 0){c = 0;}
		if(d < 0){d = 0;}
		if(a > 1){a = 1;}
		if(b > 1){b = 1;}
		if(c > 1){c = 1;}
		if(d > 1){d = 1;}
		
		int expansionLevel = a+b+c+d;
		if(a+b+c+d > 0){
		List entities = world.getEntitiesWithinAABB(EntityLivingBase.class, AxisAlignedBB.getBoundingBox(player.posX - expansionLevel, player.posY - expansionLevel, player.posZ - expansionLevel, player.posX + expansionLevel, player.posY + expansionLevel, player.posZ + expansionLevel));
		for(int i = 0; i < entities.size(); i++) {
		Object[] entityarray = entities.toArray();
		((EntityLivingBase) entityarray[i]).addPotionEffect(new PotionEffect(Potion.digSlowdown.getId(), 20));
		((EntityLivingBase) entityarray[i]).addPotionEffect(new PotionEffect(Potion.moveSlowdown.getId(), 20));
		}
		player.addPotionEffect(new PotionEffect(Potion.fireResistance.getId(), 20));
		if((player.getDataWatcher().getWatchableObjectByte(0) & 1 << 0) != 0){
			tickCounter = tickCounter + 1;
			if(tickCounter >= 40){
				//world.spawnParticle("dripWater", player.posX, player.posY, player.posZ, 0, 5, 0);//todo make this part work right, the !ifRemote can be adjusted around this part later.
				tickCounter = tickCounter - 40;
				if(a == 1){
					helmet.damageItem(4-(b+c+d), player);
					if(helmet.getItemDamage() <= 0){
						a = 0;
						player.inventory.armorInventory[3] = null;
					}
				}
				if(b == 1){
					plate.damageItem(4-(a+c+d), player);
					if(plate.getItemDamage() <= 0){
						b = 0;
						player.inventory.armorInventory[2] = null;
					}
				}
				if(c == 1){
					legs.damageItem(4-(b+a+d), player);
					if(legs.getItemDamage() <= 0){
						c = 0;
						player.inventory.armorInventory[1] = null;
					}
				}
				if(d == 1){
					boots.damageItem(4-(b+c+a), player);
					if(boots.getItemDamage() <= 0){
						d = 0;
						player.inventory.armorInventory[0] = null;
					}
				}
			}
		}
		}
	}}
}
