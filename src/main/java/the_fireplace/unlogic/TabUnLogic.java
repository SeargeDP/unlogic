package the_fireplace.unlogic;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class TabUnLogic extends CreativeTabs{

	public TabUnLogic(String par2Str) {
		super(par2Str);
	}
	@Override
	public Item getTabIconItem() {
		return Item.getItemFromBlock(unbase.UnLogicGemBlock);
	}
}
