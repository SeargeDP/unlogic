package the_fireplace.unlogic.proxy;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.entities.DJACOB;
import the_fireplace.unlogic.entities.EntityNeedle;
import the_fireplace.unlogic.entities.F1repl4ce;
import the_fireplace.unlogic.entities.SnackAttackMan;
import the_fireplace.unlogic.models.ModelFatMan;
import the_fireplace.unlogic.models.ModelNeedle;
import the_fireplace.unlogic.renderers.RenderDJACOB;
import the_fireplace.unlogic.renderers.RenderF1repl4ce;
import the_fireplace.unlogic.renderers.RenderNeedle;
import the_fireplace.unlogic.renderers.RenderSnackAttackMan;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelBlaze;
import net.minecraft.client.model.ModelPig;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy{
	@Override
	public void registerRenderers() {
	RenderingRegistry.registerEntityRenderingHandler(F1repl4ce.class, new RenderF1repl4ce(new ModelBlaze(), 0.0F));
	RenderingRegistry.registerEntityRenderingHandler(DJACOB.class, new RenderDJACOB(new ModelPig(), new ModelPig(), 0.5F));
	RenderingRegistry.registerEntityRenderingHandler(SnackAttackMan.class, new RenderSnackAttackMan(new ModelFatMan(), 1.0F));
	RenderingRegistry.registerEntityRenderingHandler(EntityNeedle.class, new RenderNeedle(new ModelNeedle()));
	}
	@Override
	public void registerGUIs(){
		unbase.registerClientInfo();
	}
}
