package the_fireplace.unlogic.animations.Lizard;

import the_fireplace.fireplacecore.MCACommonLibrary.animation.Channel;
import the_fireplace.fireplacecore.MCACommonLibrary.animation.KeyFrame;
import the_fireplace.fireplacecore.MCACommonLibrary.math.Quaternion;
import the_fireplace.fireplacecore.MCACommonLibrary.math.Vector3f;

public class ChannelMidTailSwish extends Channel {
	public ChannelMidTailSwish(String _name, float _fps, int _totalFrames, byte _mode) {
		super(_name, _fps, _totalFrames, _mode);
	}

	@Override
	protected void initializeAllFrames() {
KeyFrame frame0 = new KeyFrame();
frame0.modelRenderersRotations.put("TailMid", new Quaternion(0.0F, 0.0F, 0.0F, 1.0F));
frame0.modelRenderersTranslations.put("TailMid", new Vector3f(2.0F, 0.0F, 1.0F));
keyFrames.put(0, frame0);

KeyFrame frame39 = new KeyFrame();
frame39.modelRenderersRotations.put("TailMid", new Quaternion(0.0F, 0.0F, 0.0F, 1.0F));
frame39.modelRenderersTranslations.put("TailMid", new Vector3f(2.0F, 0.0F, 1.0F));
keyFrames.put(39, frame39);

KeyFrame frame10 = new KeyFrame();
frame10.modelRenderersRotations.put("TailMid", new Quaternion(0.0F, 0.02617695F, 0.0F, 0.99965733F));
frame10.modelRenderersTranslations.put("TailMid", new Vector3f(2.0F, 0.0F, 1.0F));
keyFrames.put(10, frame10);

KeyFrame frame30 = new KeyFrame();
frame30.modelRenderersRotations.put("TailMid", new Quaternion(0.0F, -0.02617695F, 0.0F, 0.99965733F));
frame30.modelRenderersTranslations.put("TailMid", new Vector3f(2.0F, 0.0F, 1.0F));
keyFrames.put(30, frame30);

}
}