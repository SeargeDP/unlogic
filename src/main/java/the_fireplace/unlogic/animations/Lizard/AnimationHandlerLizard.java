package the_fireplace.unlogic.animations.Lizard;

import java.util.HashMap;

import the_fireplace.fireplacecore.MCACommonLibrary.IMCAnimatedEntity;
import the_fireplace.fireplacecore.MCACommonLibrary.animation.AnimationHandler;
import the_fireplace.fireplacecore.MCACommonLibrary.animation.Channel;


public class AnimationHandlerLizard extends AnimationHandler {
	/** Map with all the animations. */
	public static HashMap<String, Channel> animChannels = new HashMap<String, Channel>();static
{
animChannels.put("Bob Head", new ChannelBobHead("Bob Head", 2.0F, 9, Channel.LOOP));
animChannels.put("Leg Movement", new ChannelLegMovement("Leg Movement", 41.0F, 41, Channel.LINEAR));
animChannels.put("Base Tail Swish", new ChannelBaseTailSwish("Base Tail Swish", 5.0F, 40, Channel.LINEAR));
animChannels.put("Mid Tail Swish", new ChannelMidTailSwish("Mid Tail Swish", 10.0F, 40, Channel.LINEAR));
animChannels.put("Tail Tip Swish", new ChannelTailTipSwish("Tail Tip Swish", 20.0F, 40, Channel.LINEAR));
}
	public AnimationHandlerLizard(IMCAnimatedEntity entity) {
		super(entity);
	}

	@Override
	public void activateAnimation(String name, float startingFrame) {
		super.activateAnimation(animChannels, name, startingFrame);
	}

	@Override
	public void stopAnimation(String name) {
		super.stopAnimation(animChannels, name);
	}

	@Override
	public void fireAnimationEventClientSide(Channel anim, Float frame) {
	}

	@Override
	public void fireAnimationEventServerSide(Channel anim, Float frame) {
	}}