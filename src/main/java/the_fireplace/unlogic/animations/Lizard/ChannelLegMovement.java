package the_fireplace.unlogic.animations.Lizard;

import the_fireplace.fireplacecore.MCACommonLibrary.animation.Channel;
import the_fireplace.fireplacecore.MCACommonLibrary.animation.KeyFrame;
import the_fireplace.fireplacecore.MCACommonLibrary.math.Quaternion;
import the_fireplace.fireplacecore.MCACommonLibrary.math.Vector3f;


public class ChannelLegMovement extends Channel {
	public ChannelLegMovement(String _name, float _fps, int _totalFrames, byte _mode) {
		super(_name, _fps, _totalFrames, _mode);
	}

	@Override
	protected void initializeAllFrames() {
KeyFrame frame0 = new KeyFrame();
frame0.modelRenderersRotations.put("Leg3", new Quaternion(-5.7054814E-9F, 0.1305262F, 0.9914449F, -4.3337433E-8F));
frame0.modelRenderersRotations.put("Leg4", new Quaternion(-5.7054814E-9F, 0.1305262F, 0.9914449F, -4.3337433E-8F));
frame0.modelRenderersRotations.put("Leg2", new Quaternion(5.7054814E-9F, -0.1305262F, 0.9914449F, -4.3337433E-8F));
frame0.modelRenderersRotations.put("Leg1", new Quaternion(5.7054814E-9F, -0.1305262F, 0.9914449F, -4.3337433E-8F));
frame0.modelRenderersTranslations.put("Leg3", new Vector3f(2.0F, 2.0F, 8.0F));
frame0.modelRenderersTranslations.put("Leg4", new Vector3f(14.0F, 2.0F, 7.0F));
frame0.modelRenderersTranslations.put("Leg2", new Vector3f(14.0F, 2.0F, -1.0F));
frame0.modelRenderersTranslations.put("Leg1", new Vector3f(2.0F, 2.0F, -1.0F));
keyFrames.put(0, frame0);

KeyFrame frame20 = new KeyFrame();
frame20.modelRenderersRotations.put("Leg3", new Quaternion(-5.7054814E-9F, 0.1305262F, 0.9914449F, -4.3337433E-8F));
frame20.modelRenderersRotations.put("Leg4", new Quaternion(-5.7054814E-9F, 0.1305262F, 0.9914449F, -4.3337433E-8F));
frame20.modelRenderersRotations.put("Leg2", new Quaternion(5.7054814E-9F, -0.1305262F, 0.9914449F, -4.3337433E-8F));
frame20.modelRenderersRotations.put("Leg1", new Quaternion(5.7054814E-9F, -0.1305262F, 0.9914449F, -4.3337433E-8F));
frame20.modelRenderersTranslations.put("Leg3", new Vector3f(2.0F, 2.0F, 8.0F));
frame20.modelRenderersTranslations.put("Leg4", new Vector3f(14.0F, 2.0F, 7.0F));
frame20.modelRenderersTranslations.put("Leg2", new Vector3f(14.0F, 2.0F, -1.0F));
frame20.modelRenderersTranslations.put("Leg1", new Vector3f(2.0F, 2.0F, -1.0F));
keyFrames.put(20, frame20);

KeyFrame frame40 = new KeyFrame();
frame40.modelRenderersRotations.put("Leg3", new Quaternion(-5.7054814E-9F, 0.1305262F, 0.9914449F, -4.3337433E-8F));
frame40.modelRenderersRotations.put("Leg4", new Quaternion(-5.7054814E-9F, 0.1305262F, 0.9914449F, -4.3337433E-8F));
frame40.modelRenderersRotations.put("Leg2", new Quaternion(5.7054814E-9F, -0.1305262F, 0.9914449F, -4.3337433E-8F));
frame40.modelRenderersRotations.put("Leg1", new Quaternion(5.7054814E-9F, -0.1305262F, 0.9914449F, -4.3337433E-8F));
frame40.modelRenderersTranslations.put("Leg3", new Vector3f(2.0F, 2.0F, 8.0F));
frame40.modelRenderersTranslations.put("Leg4", new Vector3f(14.0F, 2.0F, 7.0F));
frame40.modelRenderersTranslations.put("Leg2", new Vector3f(14.0F, 2.0F, -1.0F));
frame40.modelRenderersTranslations.put("Leg1", new Vector3f(2.0F, 2.0F, -1.0F));
keyFrames.put(40, frame40);

KeyFrame frame10 = new KeyFrame();
frame10.modelRenderersRotations.put("Leg3", new Quaternion(0.0113761155F, 0.13002951F, 0.98767215F, 0.08641018F));
frame10.modelRenderersRotations.put("Leg4", new Quaternion(0.0113761155F, -0.13002951F, -0.98767215F, 0.08641018F));
frame10.modelRenderersRotations.put("Leg2", new Quaternion(-0.0113761155F, -0.13002951F, 0.98767215F, 0.08641018F));
frame10.modelRenderersRotations.put("Leg1", new Quaternion(-0.0113761155F, 0.13002951F, -0.98767215F, 0.08641018F));
frame10.modelRenderersTranslations.put("Leg3", new Vector3f(2.0F, 2.0F, 8.0F));
frame10.modelRenderersTranslations.put("Leg4", new Vector3f(14.0F, 2.0F, 7.0F));
frame10.modelRenderersTranslations.put("Leg2", new Vector3f(14.0F, 2.0F, -1.0F));
frame10.modelRenderersTranslations.put("Leg1", new Vector3f(2.0F, 2.0F, -1.0F));
keyFrames.put(10, frame10);

KeyFrame frame30 = new KeyFrame();
frame30.modelRenderersRotations.put("Leg3", new Quaternion(0.0113761155F, -0.13002951F, -0.98767215F, 0.08641018F));
frame30.modelRenderersRotations.put("Leg4", new Quaternion(0.0113761155F, 0.13002951F, 0.98767215F, 0.08641018F));
frame30.modelRenderersRotations.put("Leg2", new Quaternion(-0.0113761155F, 0.13002951F, -0.98767215F, 0.08641018F));
frame30.modelRenderersRotations.put("Leg1", new Quaternion(-0.0113761155F, -0.13002951F, 0.98767215F, 0.08641018F));
frame30.modelRenderersTranslations.put("Leg3", new Vector3f(2.0F, 2.0F, 8.0F));
frame30.modelRenderersTranslations.put("Leg4", new Vector3f(14.0F, 2.0F, 7.0F));
frame30.modelRenderersTranslations.put("Leg2", new Vector3f(14.0F, 2.0F, -1.0F));
frame30.modelRenderersTranslations.put("Leg1", new Vector3f(2.0F, 2.0F, -1.0F));
keyFrames.put(30, frame30);

}
}