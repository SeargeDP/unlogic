package the_fireplace.unlogic.items;

import the_fireplace.unlogic.unbase;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class NetherCleanser extends Item{
	public NetherCleanser(){
        setCreativeTab(unbase.TabUnLogic);
		maxStackSize = 1;
		setMaxDamage(250);
		setNoRepair();
		setUnlocalizedName("NetherCleanser");
		setTextureName("unlogic:NCleanser");
	}
	@Override
	public boolean hasContainerItem(){//change to getContainerItem()
		return true;
	}
	@Override
	public boolean doesContainerItemLeaveCraftingGrid(ItemStack itemstack) {
		return false;
	}
	@Override
	public ItemStack getContainerItem(ItemStack itemStack) {
		itemStack.setItemDamage(itemStack.getItemDamage() + 1);
		return itemStack;
	}
}
