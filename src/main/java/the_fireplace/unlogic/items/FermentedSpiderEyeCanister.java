package the_fireplace.unlogic.items;

import java.util.Random;

import the_fireplace.unlogic.unbase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class FermentedSpiderEyeCanister extends Item{
	public FermentedSpiderEyeCanister(){
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("Canister");
		setMaxStackSize(1);
		this.setMaxDamage(256);
		this.setNoRepair();
	}
	@Override
	public boolean hasContainerItem(){
		return true;
	}
	@Override
	public boolean doesContainerItemLeaveCraftingGrid(ItemStack itemstack) {
		return false;
	}
	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int par7, float xFloat, float yFloat, float zFloat){
		if (!player.canPlayerEdit(x, y, z, par7, itemStack))
		{
			return false;
		}else if(world.getBlock(x, y, z) == Blocks.cactus){
		world.setBlock(x, y, z, unbase.StrippedCactus, 0, 2);
		itemStack.setItemDamage(itemStack.getItemDamage() + 1);
		//disable further indented code when in creative mode
			Random rand = new Random();
			int count = rand.nextInt(12) + 1;
		
			player.inventory.addItemStackToInventory(new ItemStack(unbase.Needle, count));
		
        return true;
		}
		
        return true;
    }
	@Override
	public ItemStack getContainerItem(ItemStack itemStack) {
		itemStack.setItemDamage(itemStack.getItemDamage() + 1);
		return itemStack;
	}
}
