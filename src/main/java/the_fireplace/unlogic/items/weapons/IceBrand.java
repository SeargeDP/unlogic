package the_fireplace.unlogic.items.weapons;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.ItemSword;

public class IceBrand extends ItemSword {

	public IceBrand(ToolMaterial p_i45356_1_) {
		super(p_i45356_1_);
		setUnlocalizedName("IceBrand");
		setTextureName("unlogic:ice_brand");
		setCreativeTab(unbase.TabUnLogic);
	}

}
