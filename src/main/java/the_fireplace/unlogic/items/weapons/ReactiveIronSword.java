package the_fireplace.unlogic.items.weapons;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.entities.*;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.player.EntityPlayer;

public class ReactiveIronSword extends ItemSword {

	public ReactiveIronSword(ToolMaterial p_i45356_1_) {
		super(p_i45356_1_);
		setUnlocalizedName("ReactiveIronSword");
		setTextureName("minecraft:iron_sword");
		setCreativeTab(unbase.TabUnLogic);
		}
	@Override
    @SideOnly(Side.CLIENT)
    public boolean hasEffect(ItemStack stack)//gives it the glowy enchanted look
    {
        return true;
    }
	private int storeddamage;
	/*@Override
	public boolean hitEntity(ItemStack is, EntityLivingBase player, EntityLivingBase target)
	{
		EntityPlayer targetPlayer;
		//targetPlayer = (EntityPlayer) player;//Causes crash
		is.damageItem(1, target);
		storeddamage = is.getItemDamage();
			if(target instanceof EntityBlaze || target instanceof EntityMagmaCube || target instanceof F1repl4ce){
				is.stackSize = 0;
				targetPlayer.inventory.setInventorySlotContents(1, new ItemStack(unbase.FireBrand, 1, storeddamage));
			}
		
		return true;
	}*/
}
