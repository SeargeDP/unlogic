package the_fireplace.unlogic.items.components;

import java.util.List;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.entities.EntityEnderPearlDust;
import the_fireplace.unlogic.entities.EntityUnlogicGem;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class UnlogicGem extends ItemFood{
	
	public UnlogicGem(int p_i45340_1_, boolean p_i45340_2_) {
		super(p_i45340_1_, p_i45340_2_);
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("UnlogicGem");
	}
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player,
            List list, boolean par4) {
		list.add("To create, throw "+StatCollector.translateToLocal("item.UnlogicEnderPearlDust.name")+",");
		list.add(StatCollector.translateToLocal("item.redstone.name")+",");
		list.add("and "+StatCollector.translateToLocal("item.yellowDust.name"));
		list.add("on the ground together.");
	}
	@Override
	public boolean hasCustomEntity(ItemStack stack)
    {
        return true;
    }
	@Override
	public Entity createEntity(World world, Entity location, ItemStack itemstack)
    {
		Entity newentity = new EntityUnlogicGem(world, location.posX, location.posY, location.posZ, itemstack);
        return newentity;
    }
}
