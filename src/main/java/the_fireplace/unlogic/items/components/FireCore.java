package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class FireCore extends Item{
	public FireCore(){
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("FireCore");
		this.maxStackSize = 1;
		this.setMaxDamage(4);
		this.setNoRepair();
	}
	@Override
	public boolean hasContainerItem(){
		return true;
	}
	@Override
	public boolean doesContainerItemLeaveCraftingGrid(ItemStack itemstack) {
		return false;
	}
	@Override
	public ItemStack getContainerItem(ItemStack itemStack) {
		itemStack.setItemDamage(itemStack.getItemDamage() + 1);
		return itemStack;
	}
}
