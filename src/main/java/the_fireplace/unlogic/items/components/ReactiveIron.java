package the_fireplace.unlogic.items.components;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import the_fireplace.unlogic.unbase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

public class ReactiveIron extends Item {
	public ReactiveIron(){
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("ReactiveIron");
		setTextureName("iron_ingot");
	}
	@Override
    @SideOnly(Side.CLIENT)
    public boolean hasEffect(ItemStack stack)//gives it the glowy enchanted look
    {
        return true;
    }
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player,
            List list, boolean par4) {
		list.add("To create, throw an");
		list.add(StatCollector.translateToLocal("item.ingotIron.name")+" on the");
		list.add("ground with an "+StatCollector.translateToLocal("item.UnlogicGem.name")+".");
	}
}
