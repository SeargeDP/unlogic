package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.Item;

public class LargeBurntFlesh extends Item{
	public LargeBurntFlesh(){
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("LargeBurntFlesh");
	}
}
