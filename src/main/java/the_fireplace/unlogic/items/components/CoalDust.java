package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.Item;

public class CoalDust extends Item{
public CoalDust(){
	setTextureName("unlogic:CoalDust");
	setUnlocalizedName("CoalDust");
	setCreativeTab(unbase.TabUnLogic);
}
}
