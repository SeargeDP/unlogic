package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;

public class BurntFlesh extends ItemFood{
	public BurntFlesh() {
		super(1, true);
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("BurntFlesh");
	}
}
