package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;

public class BurntMeat extends ItemFood{
	public BurntMeat(){
		super(1, true);
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("BurntMeat");
	}
}
