package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class NeedleBundle extends Item {
	@Override
	public boolean onItemUse(ItemStack tool,
			EntityPlayer player, World world, int x, int y,
			int z, int par7, float xFloat, float yFloat, float zFloat)
    {
		if (!player.canPlayerEdit(x, y, z, par7, tool))
		{
			return false;
		}else if(world.getBlock(x, y, z) == unbase.StrippedCactus){
		world.setBlock(x, y, z, Blocks.cactus, 0, 2);

        if (!player.capabilities.isCreativeMode)
        {
		player.inventory.consumeInventoryItem(this);
        }
        return true;
		}
        return true;
    }
}
