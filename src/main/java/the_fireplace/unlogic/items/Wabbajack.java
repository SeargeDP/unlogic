package the_fireplace.unlogic.items;

import java.util.Random;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.entities.DJACOB;
import the_fireplace.unlogic.entities.EntityGoldBomb;
import the_fireplace.unlogic.entities.EntityNeedle;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.item.EntityEnderPearl;
import net.minecraft.entity.item.EntityExpBottle;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityEgg;
import net.minecraft.entity.projectile.EntityLargeFireball;
import net.minecraft.entity.projectile.EntitySmallFireball;
import net.minecraft.entity.projectile.EntitySnowball;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class Wabbajack extends Item{
	public Wabbajack(){
		setTextureName("unlogic:Wabbajack");
		setUnlocalizedName("Wabbajack");
		setCreativeTab(unbase.TabUnLogic);
		setMaxDamage(128);
		setMaxStackSize(1);
	}
	@Override
	public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player){
	Random rand = new Random();
	int r = rand.nextInt(100) + 1;//minimum value is 1, max is 100. Only whole numbers. Use for the chance of each effect.
	
	if(r >= 1 && r <= 20){//20%, 1% to do with lightning bolt, Tested and Working
		world.playSoundAtEntity(player, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

        if (!world.isRemote)
        {
            world.spawnEntityInWorld(new EntityEnderPearl(world, player));
        }
        damageItem(is);
        return is;
	}
	if(r == 100 || r == 69 || (r >= 60 && r < 67)){
		world.playSoundAtEntity(player, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

        if (!world.isRemote)
        {
        	world.spawnEntityInWorld(new EntityExpBottle(world, player));
        }
        damageItem(is);
        return is;
	}
	if(r >= 20 && r <= 35){//15%, 1% to do with ender pearl, Tested and Working
        //if (!world.isRemote)//Noted out to prevent invisible lightning, it vanishes after an instant anyways
        //{
        	world.spawnEntityInWorld(new EntityLightningBolt(world, player.posX, player.posY, player.posZ));
       // }
            damageItem(is);
        return is;
	}
	if(r >=35 && r <= 40){//5%, 1% to do with lightning
        if (!world.isRemote)
        {
        	world.spawnEntityInWorld(new EntityGoldBomb(world, player.posX, player.posY, player.posZ, player));
        }
        damageItem(is);
        return is;
	}
	if(r >= 40 && r <= 50){//10%, 1% to do with Gold Bomb Explosion
        if (!world.isRemote)
        {
        	Entity entity = new EntityTNTPrimed(world, player.posX, player.posY, player.posZ, player);
        	entity.setPosition(player.posX, player.posY + (r/10), player.posZ);
            world.spawnEntityInWorld(entity);
        }
        damageItem(is);
        return is;
	}
	if(r > 50 && r < 60){//Tested and Working
		player.jump();
        damageItem(is);
		return is;
	}
	if(r >= 68 && r <= 70){//2%
		
        if (!world.isRemote)
        {
        	Entity entity = new DJACOB(world, player.posX, player.posY, player.posZ);
        	entity.setPosition(player.posX, player.posY, player.posZ);
            world.spawnEntityInWorld(entity);
        }
        damageItem(is);
        return is;
	}
	if(r >= 71 && r <= 99){//28%

        if (!world.isRemote)
        {
        	Entity entity=new EntitySnowman(world);
        	if(r >=81 && r <= 90){
            	entity.setPosition(player.posX, player.posY, player.posZ);
        	}else if(r >=91 && r <= 95){
            	entity.setPosition(player.posX, player.posX, player.posZ);
        	}else{
            	entity.setPosition(player.posX, player.posZ, player.posZ);
        	}
            world.spawnEntityInWorld(entity);
        }
        damageItem(is);
        return is;
	}
	return is;
}
	private void damageItem(ItemStack is){
        is.setItemDamage(is.getItemDamage() + 1);
	}
}
