package the_fireplace.unlogic.items;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class HandheldCrusher extends Item{

	public HandheldCrusher() {
		super();
		this.maxStackSize = 1;
		this.setMaxDamage(1024);
		this.setNoRepair();
		setTextureName("unlogic:crusher");
	}
@Override
public boolean hasContainerItem(){
	return true;
}
@Override
public boolean doesContainerItemLeaveCraftingGrid(ItemStack itemstack) {
	return false;
}
@Override
public ItemStack getContainerItem(ItemStack itemStack) {
	itemStack.setItemDamage(itemStack.getItemDamage() + 1);
	return itemStack;
}
}
