package the_fireplace.unlogic.dimension;

import net.minecraft.world.WorldProvider;

public class WorldProviderDeepNether extends WorldProvider {

	@Override
	public String getDimensionName() {
		return "DeepNether";
	}

}
