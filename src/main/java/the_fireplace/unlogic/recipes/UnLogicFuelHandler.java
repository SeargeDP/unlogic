package the_fireplace.unlogic.recipes;

import the_fireplace.unlogic.unbase;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.IFuelHandler;

public class UnLogicFuelHandler implements IFuelHandler
{
    @Override
    public int getBurnTime(ItemStack fuel)
    {
        Block var1 = Block.getBlockFromItem(fuel.getItem());
        Item var2 = fuel.getItem();

        if (var1 == unbase.KnowledgeBlock)
        {
            return 900;
        }else if (var1 == unbase.BlackScreen)
        {
            return 300;
        }else if (var1 == unbase.BlueScreen)
        {
            return 300;
        }else if (var1 == unbase.BrownScreen)
        {
            return 300;
        }else if (var1 == unbase.RedScreen)
        {
            return 300;
        }else if (var1 == unbase.OrangeScreen)
        {
            return 300;
        }else if (var1 == unbase.YellowScreen)
        {
            return 300;
        }else if (var1 == unbase.LimeScreen)
        {
            return 300;
        }else if (var1 == unbase.GreenScreen)
        {
            return 300;
        }else if (var1 == unbase.SkyScreen)
        {
            return 300;
        }else if (var1 == unbase.PinkScreen)
        {
            return 300;
        }else if (var1 == unbase.MagentaScreen)
        {
            return 300;
        }else if (var1 == unbase.PurpleScreen)
        {
            return 300;
        }else if (var1 == unbase.SilverScreen)
        {
            return 300;
        }else if (var1 == unbase.GreyScreen)
        {
            return 300;
        }else if (var1 == unbase.WhiteScreen)
        {
            return 300;
        }else if (var1 == unbase.CyanScreen)
        {
            return 300;
        }else if (var2 == unbase.ItemBlazeCake)
        {
            return 1200;
        }else if (var2 == unbase.CoalDust)
        {
            return 1600;//add reduced fuel config option
        }else if (var2 == unbase.TinyCoalDust)
        {
            return 200;//add reduced fuel config option
        }else if (var2 == unbase.TinyCoal)
        {
            return 200;
        }else
        {
            return 0;
        }
    }
}
