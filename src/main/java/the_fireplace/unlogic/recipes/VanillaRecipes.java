package the_fireplace.unlogic.recipes;

import the_fireplace.unlogic.unbase;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import cpw.mods.fml.common.registry.GameRegistry;
import the_fireplace.fireplacecore.EWAPI;
import the_fireplace.fireplacecore.config.FCCV;

public class VanillaRecipes{

	public static void addRecipes() {
		//ItemStacks
        ItemStack fireCore = new ItemStack(unbase.FireCore, 1, OreDictionary.WILDCARD_VALUE);
        ItemStack iceCore = new ItemStack(unbase.IceCore, 1, OreDictionary.WILDCARD_VALUE);
        ItemStack newFireCore = new ItemStack(unbase.FireCore);
        ItemStack newIceCore = new ItemStack(unbase.IceCore);
        ItemStack unlogicGem = new ItemStack(unbase.UnlogicGem);
        ItemStack seedStack = new ItemStack(Items.wheat_seeds);
        ItemStack epDustStack = new ItemStack(unbase.EnderPearlDust);
        ItemStack epDustStack9 = new ItemStack(unbase.EnderPearlDust, 9);
        ItemStack qSeedStack = new ItemStack(unbase.QSeed);
        ItemStack qSeedStack5 = new ItemStack(unbase.QSeed, 5);
        ItemStack quartzStack = new ItemStack(Items.quartz);
        ItemStack enderPearlStack = new ItemStack(Items.ender_pearl);
        ItemStack lavaStack = new ItemStack(Items.lava_bucket);
        ItemStack waterStack = new ItemStack(Items.water_bucket);
        ItemStack blazePowderStack = new ItemStack(Items.blaze_powder);
        ItemStack fireHelmStack = new ItemStack(unbase.FireHelmet);
        ItemStack fireChestStack = new ItemStack(unbase.FireChestplate);
        ItemStack fireBootsStack = new ItemStack(unbase.FireBoots);
        ItemStack fireLegsStack = new ItemStack(unbase.FireLeggings);
        ItemStack iceHelmStack = new ItemStack(unbase.IceHelmet);
        ItemStack iceChestStack = new ItemStack(unbase.IceChestplate);
        ItemStack iceBootsStack = new ItemStack(unbase.IceBoots);
        ItemStack iceLegsStack = new ItemStack(unbase.IceLeggings);
        ItemStack ObsidianStack = new ItemStack(Blocks.obsidian);
        ItemStack NetherrackStack = new ItemStack(Blocks.netherrack);
        ItemStack GlowstoneStack = new ItemStack(Blocks.glowstone);
        ItemStack paperStack = new ItemStack(Items.paper);
        ItemStack stringStack = new ItemStack(Items.string);
        ItemStack stringStack2 = new ItemStack(Items.string, 2);
        ItemStack crusherStack = new ItemStack(unbase.HandheldCrusher, 1, OreDictionary.WILDCARD_VALUE);
        ItemStack newCrusherStack = new ItemStack(unbase.HandheldCrusher);
        ItemStack reedStack = new ItemStack(Items.reeds);
        ItemStack flintStack = new ItemStack(Items.flint);
        ItemStack ironStack = new ItemStack(Items.iron_ingot);
        ItemStack diamondStack = new ItemStack(Items.diamond);
        ItemStack charcoalStack = new ItemStack(Items.coal, 1, 1);
        ItemStack cobbleStack = new ItemStack(Blocks.cobblestone);
        ItemStack stoneStack = new ItemStack(Blocks.stone);
        ItemStack boneStack = new ItemStack(Items.bone);
        ItemStack bonemealStack4 = new ItemStack(Items.dye, 4, 15);
        ItemStack brickHolderStack4 = new ItemStack(unbase.BrickFireHolder, 4);
        ItemStack brickStack = new ItemStack(Items.brick);
        ItemStack redstoneStack2 = new ItemStack(Items.redstone, 2);
        ItemStack lapisStack2 = new ItemStack(Items.dye, 2, 4);
        ItemStack fSpiderEyeStack = new ItemStack(Items.fermented_spider_eye);
        ItemStack fSpiderEyeStack2 = new ItemStack(Items.fermented_spider_eye, 2);
        ItemStack spiderEyeStack = new ItemStack(Items.spider_eye);
        ItemStack bMushroomStack = new ItemStack(Blocks.brown_mushroom);
        ItemStack sugarStack = new ItemStack(Items.sugar);
        ItemStack burntMeatStack = new ItemStack(unbase.BurntMeat);
        ItemStack burntFleshStack = new ItemStack(unbase.BurntFlesh);
        ItemStack lBurntFleshStack = new ItemStack(unbase.LargeBurntFlesh);
        ItemStack rottenFleshStack = new ItemStack(Items.rotten_flesh);
        ItemStack steakStack = new ItemStack(Items.cooked_beef);
        ItemStack chickenStack = new ItemStack(Items.cooked_chicken);
        ItemStack fishStack = new ItemStack(Items.cooked_fished);
        ItemStack porkStack = new ItemStack(Items.cooked_porkchop);
        ItemStack hayStack = new ItemStack(Blocks.hay_block);
        ItemStack unlogicGemBlockStack = new ItemStack(unbase.UnLogicGemBlock);
        ItemStack knowledgeBlockStack = new ItemStack(unbase.KnowledgeBlock);
        ItemStack woodStack = new ItemStack(Blocks.planks, 1, OreDictionary.WILDCARD_VALUE);
        ItemStack bookshelfStack = new ItemStack(Blocks.bookshelf);
        ItemStack bookStack = new ItemStack(Items.book);
        ItemStack writtenBookStack = new ItemStack(Items.written_book);
        ItemStack newNetherCleanserStack = new ItemStack(unbase.NetherCleanser);
        ItemStack newNetherCorruptorStack = new ItemStack(unbase.NetherCorruptor);
        ItemStack netherCleanserStack = new ItemStack(unbase.NetherCleanser, 1, OreDictionary.WILDCARD_VALUE);
        ItemStack netherCorruptorStack = new ItemStack(unbase.NetherCorruptor, 1, OreDictionary.WILDCARD_VALUE);
        ItemStack endPieceStack = new ItemStack(unbase.EndPiece);
        ItemStack netherBrickStack = new ItemStack(Blocks.nether_brick);
        ItemStack bricksStack = new ItemStack(Blocks.brick_block);
        ItemStack quartzBlockStack = new ItemStack(Blocks.quartz_block);
        ItemStack ironBlockStack = new ItemStack(Blocks.iron_block);
        ItemStack endStoneStack = new ItemStack(Blocks.end_stone);
        ItemStack ironOreStack = new ItemStack(Blocks.iron_ore);
        ItemStack netherQuartzOreStack = new ItemStack(Blocks.quartz_ore);
        ItemStack fenceStack = new ItemStack(Blocks.fence);
        ItemStack nbFenceStack = new ItemStack(Blocks.nether_brick_fence);
        ItemStack brickStairsStack = new ItemStack(Blocks.brick_stairs);
        ItemStack nbStairsStack = new ItemStack(Blocks.nether_brick_stairs);
        ItemStack magmaCreamStack = new ItemStack(Items.magma_cream);
        ItemStack blazeRodStack = new ItemStack(Items.blaze_rod);
        ItemStack netherrackBrickStack = new ItemStack(Items.netherbrick);
        ItemStack netherQuartzStack = new ItemStack(Items.quartz);
        ItemStack bottleStack = new ItemStack(Items.glass_bottle);
        ItemStack netherWartStack = new ItemStack(Items.nether_wart);
        ItemStack coalBlockStack = new ItemStack(Blocks.coal_block);
        ItemStack cobblestoneStack = new ItemStack(Blocks.cobblestone);
		ItemStack BlazeCakeStack = new ItemStack(unbase.ItemBlazeCake);
		ItemStack ChocolateCakeStack = new ItemStack(unbase.ItemChocolateCake);
		ItemStack BlazePowderStack = new ItemStack(Items.blaze_powder);
		ItemStack BlazePowderStack3 = new ItemStack(Items.blaze_powder, 3);
		ItemStack SlimeballStack = new ItemStack(Items.slime_ball);
		ItemStack CocoaBeansStack = new ItemStack(Items.dye, 1, 3);
		ItemStack CakeStack = new ItemStack(Items.cake);
		ItemStack MagmaCreamStack = new ItemStack(Items.magma_cream);
		ItemStack snowStack = new ItemStack(Blocks.snow);
		ItemStack canisterStack = new ItemStack(unbase.Canister);
		ItemStack NeedleStack = new ItemStack(unbase.Needle);
		ItemStack NeedleStack9 = new ItemStack(unbase.Needle, 9);
		ItemStack NeedleBundleStack = new ItemStack(unbase.NeedleBundle);
		ItemStack NeedleBundleStack4 = new ItemStack(unbase.NeedleBundle, 4);
		ItemStack NeedleBlockStack = new ItemStack(unbase.NeedleBlock);
		ItemStack CactusStack = new ItemStack(Blocks.cactus);
		ItemStack StrippedCactusStack = new ItemStack(unbase.StrippedCactus);
		ItemStack LeatherStack = new ItemStack(Items.leather);
		ItemStack CoalDustStack = new ItemStack(unbase.CoalDust);
		ItemStack TinyCoalDustStack = new ItemStack(unbase.TinyCoalDust);
		ItemStack TinyCoalDustStack8 = new ItemStack(unbase.TinyCoalDust, 8);
		ItemStack TinyCoalStack = new ItemStack(unbase.TinyCoal);
		ItemStack TinyCoalStack8 = new ItemStack(unbase.TinyCoal, 8);
		ItemStack CoalStack = new ItemStack(Items.coal);
		ItemStack WeakSlimeStack = new ItemStack(unbase.WeakSlime);
		ItemStack WeakSlimeStack4 = new ItemStack(unbase.WeakSlime, 4);
		ItemStack WeakTorchStack2 = new ItemStack(unbase.WeakTorch, 2);
		ItemStack StickStack = new ItemStack(Items.stick);
		ItemStack woolStack = new ItemStack(Blocks.wool, 1, OreDictionary.WILDCARD_VALUE);
		ItemStack ObsidianRodStack = new ItemStack(unbase.ObsidianRod);
		ItemStack WabbajackStack = new ItemStack(unbase.Wabbajack);
		ItemStack ReactiveIronStack = new ItemStack(unbase.ReactiveIron);
		ItemStack GoldBombStack = new ItemStack(unbase.GoldBomb);
		ItemStack GoldStack = new ItemStack(Items.gold_ingot);
		ItemStack AntiGoldStack = new ItemStack(unbase.AntiGold);
		ItemStack ObsidianToolStack = new ItemStack(unbase.ObsidianTool);
		//Flowers
		ItemStack smYellowStack = new ItemStack(Blocks.yellow_flower, 1, 0);//dandelion
		ItemStack smRedStack = new ItemStack(Blocks.red_flower, 1, 0);//rose(poppy)
		ItemStack smSkyStack = new ItemStack(Blocks.red_flower, 1, 1);//blue orchid
		ItemStack smMagentaStack = new ItemStack(Blocks.red_flower, 1, 2);//allium
		ItemStack smSilverStack = new ItemStack(Blocks.red_flower, 1, 3);//Houstenia(Azure Bluet)
		ItemStack smRed2Stack = new ItemStack(Blocks.red_flower, 1, 4);//red tulip
		ItemStack smOrangeStack = new ItemStack(Blocks.red_flower, 1, 5);//orange tulip
		ItemStack smSilver2Stack = new ItemStack(Blocks.red_flower, 1, 6);//white tulip
		ItemStack smPinkStack = new ItemStack(Blocks.red_flower, 1, 7);//pink tulip
		ItemStack smSilver3Stack = new ItemStack(Blocks.red_flower, 1, 8);//Oxeye Daisy
		ItemStack lgYellowStack = new ItemStack(Blocks.double_plant, 1, 0);//sunflower
		ItemStack lgMagentaStack = new ItemStack(Blocks.double_plant, 1, 1);//lilac
		ItemStack lgRedStack = new ItemStack(Blocks.double_plant, 1, 4);//rose bush
		ItemStack lgPinkStack = new ItemStack(Blocks.double_plant, 1, 5);//peony
        //Dyes
        ItemStack BlackDyeStack = new ItemStack(Items.dye, 1, 0);
        ItemStack BlackDyeStack9 = new ItemStack(Items.dye, 9, 0);
        ItemStack RedDyeStack = new ItemStack(Items.dye, 1, 1);
        ItemStack RedDyeStack2 = new ItemStack(Items.dye, 2, 1);
        ItemStack RedDyeStack4 = new ItemStack(Items.dye, 4, 1);
        ItemStack RedDyeStack9 = new ItemStack(Items.dye, 9, 1);
        ItemStack GreenDyeStack = new ItemStack(Items.dye, 1, 2);
        ItemStack GreenDyeStack9 = new ItemStack(Items.dye, 9, 2);
        ItemStack BrownDyeStack = new ItemStack(Items.dye, 1, 3);
        ItemStack BrownDyeStack9 = new ItemStack(Items.dye, 9, 3);
        ItemStack BlueDyeStack = new ItemStack(Items.dye, 1, 4);
        ItemStack BlueDyeStack9 = new ItemStack(Items.dye, 9, 4);
        ItemStack PurpleDyeStack = new ItemStack(Items.dye, 1, 5);
        ItemStack PurpleDyeStack9 = new ItemStack(Items.dye, 9, 5);
        ItemStack CyanDyeStack = new ItemStack(Items.dye, 1, 6);
        ItemStack CyanDyeStack9 = new ItemStack(Items.dye, 9, 6);
        ItemStack SilverDyeStack = new ItemStack(Items.dye, 1, 7);
        ItemStack SilverDyeStack2 = new ItemStack(Items.dye, 2, 7);
        ItemStack SilverDyeStack9 = new ItemStack(Items.dye, 9, 7);
        ItemStack GreyDyeStack = new ItemStack(Items.dye, 1, 8);
        ItemStack GreyDyeStack9 = new ItemStack(Items.dye, 9, 8);
        ItemStack PinkDyeStack = new ItemStack(Items.dye, 1, 9);
        ItemStack PinkDyeStack2 = new ItemStack(Items.dye, 2, 9);
        ItemStack PinkDyeStack4 = new ItemStack(Items.dye, 4, 9);
        ItemStack PinkDyeStack9 = new ItemStack(Items.dye, 9, 9);
        ItemStack LimeDyeStack = new ItemStack(Items.dye, 1, 10);
        ItemStack LimeDyeStack9 = new ItemStack(Items.dye, 9, 10);
        ItemStack YellowDyeStack = new ItemStack(Items.dye, 1, 11);
        ItemStack YellowDyeStack2 = new ItemStack(Items.dye, 2, 11);
        ItemStack YellowDyeStack4 = new ItemStack(Items.dye, 4, 11);
        ItemStack YellowDyeStack9 = new ItemStack(Items.dye, 9, 11);
        ItemStack SkyDyeStack = new ItemStack(Items.dye, 1, 12);
        ItemStack SkyDyeStack2 = new ItemStack(Items.dye, 2, 12);
        ItemStack SkyDyeStack9 = new ItemStack(Items.dye, 9, 12);
        ItemStack MagentaDyeStack = new ItemStack(Items.dye, 1, 13);
        ItemStack MagentaDyeStack2 = new ItemStack(Items.dye, 2, 13);
        ItemStack MagentaDyeStack4 = new ItemStack(Items.dye, 4, 13);
        ItemStack MagentaDyeStack9 = new ItemStack(Items.dye, 9, 13);
        ItemStack OrangeDyeStack = new ItemStack(Items.dye, 1, 14);
        ItemStack OrangeDyeStack2 = new ItemStack(Items.dye, 2, 14);
        ItemStack OrangeDyeStack9 = new ItemStack(Items.dye, 9, 14);
        ItemStack BoneMealStack = new ItemStack(Items.dye, 1, 15);
        ItemStack BoneMealStack9 = new ItemStack(Items.dye, 9, 15);
        //Canvas Block Stacks
        ItemStack CStack0 = new ItemStack(unbase.BlackScreen, 4);
        ItemStack CStack1 = new ItemStack(unbase.RedScreen, 4);
        ItemStack CStack2 = new ItemStack(unbase.GreenScreen, 4);
        ItemStack CStack3 = new ItemStack(unbase.BrownScreen, 4);
        ItemStack CStack4 = new ItemStack(unbase.BlueScreen, 4);
        ItemStack CStack5 = new ItemStack(unbase.PurpleScreen, 4);
        ItemStack CStack6 = new ItemStack(unbase.CyanScreen, 4);
        ItemStack CStack7 = new ItemStack(unbase.SilverScreen, 4);
        ItemStack CStack8 = new ItemStack(unbase.GreyScreen, 4);
        ItemStack CStack9 = new ItemStack(unbase.PinkScreen, 4);
        ItemStack CStack10 = new ItemStack(unbase.LimeScreen, 4);
        ItemStack CStack11 = new ItemStack(unbase.YellowScreen, 4);
        ItemStack CStack12 = new ItemStack(unbase.SkyScreen, 4);
        ItemStack CStack13 = new ItemStack(unbase.MagentaScreen, 4);
        ItemStack CStack14 = new ItemStack(unbase.OrangeScreen, 4);
        ItemStack CStack15 = new ItemStack(unbase.WhiteScreen, 4);
        //Deprecated Content Stacks
        //Remove in 1.5
        	ItemStack LStack0 = new ItemStack(unbase.LBlackDye);
        	ItemStack LStack1 = new ItemStack(unbase.LRedDye);
        	ItemStack LStack2 = new ItemStack(unbase.LGreenDye);
        	ItemStack LStack3 = new ItemStack(unbase.LBrownDye);
        	ItemStack LStack4 = new ItemStack(unbase.LBlueDye);
        	ItemStack LStack5 = new ItemStack(unbase.LPurpleDye);
        	ItemStack LStack6 = new ItemStack(unbase.LCyanDye);
        	ItemStack LStack7 = new ItemStack(unbase.LSilverDye);
        	ItemStack LStack8 = new ItemStack(unbase.LGreyDye);
        	ItemStack LStack9 = new ItemStack(unbase.LPinkDye);
        	ItemStack LStack10 = new ItemStack(unbase.LLimeDye);
        	ItemStack LStack11 = new ItemStack(unbase.LYellowDye);
        	ItemStack LStack12 = new ItemStack(unbase.LSkyDye);
        	ItemStack LStack13 = new ItemStack(unbase.LMagentaDye);
        	ItemStack LStack14 = new ItemStack(unbase.LOrangeDye);
        	ItemStack LStack15 = new ItemStack(unbase.LWhiteDye);
        //Remove in 1.6
        	ItemStack redGemStack = new ItemStack(unbase.RedGem);
        	ItemStack blueGemStack = new ItemStack(unbase.BlueGem);
        	ItemStack redGemStack9 = new ItemStack(unbase.RedGem, 9);
        	ItemStack blueGemStack9 = new ItemStack(unbase.BlueGem, 9);
            ItemStack redGemBlockStack = new ItemStack(unbase.RedGemBlock);
            ItemStack blueGemBlockStack = new ItemStack(unbase.BlueGemBlock);
        //Recipes
        GameRegistry.addRecipe(WabbajackStack, "  y", " x ", "x  ",
        		'x', ObsidianRodStack, 'y', unlogicGem);
        if(FCCV.CHEAT1 == "MegaQCrop" || FCCV.CHEAT2 == "MegaQCrop" || FCCV.CHEAT3 == "MegaQCrop" || FCCV.CHEAT4 == "MegaQCrop" || FCCV.CHEAT5 == "MegaQCrop"){
        	GameRegistry.addRecipe(qSeedStack5, "sus", "uqu", "sus",
            		's', seedStack, 'q', unlogicGem, 'u', quartzStack);
        }else{
        GameRegistry.addRecipe(qSeedStack, "sus", "uqu", "sus",
        		's', seedStack, 'q', unlogicGem, 'u', quartzStack);
        }
        GameRegistry.addRecipe(newFireCore, "xyx", "yzy", "xyx",
        		'z', lavaStack, 'y', blazePowderStack, 'x', epDustStack);
        GameRegistry.addRecipe(newIceCore, "xyx", "yzy", "xyx",
        		'z', waterStack, 'y', snowStack, 'x', epDustStack);
        GameRegistry.addRecipe(canisterStack, "iri", "ifi", "iii",
        		'i', ironStack, 'f', fSpiderEyeStack, 'r', RedDyeStack);
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(canisterStack, new Object[]{"iri", "ifi", "iii",
        		'i', "ingotTin", 'r', "dyeRed", 'f', fSpiderEyeStack}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(canisterStack, new Object[]{"iri", "ifi", "iii",
        		'i', "ingotInvar", 'r', "dyeRed", 'f', fSpiderEyeStack}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(canisterStack, new Object[]{"iri", "ifi", "iii",
        		'i', "ingotTitanium", 'r', "dyeRed", 'f', fSpiderEyeStack}));
        GameRegistry.addRecipe(fireHelmStack, "ddd", "oco",
        		'o', ObsidianStack, 'd', ReactiveIronStack, 'c', fireCore);
        GameRegistry.addRecipe(fireChestStack, "o o", "ncn", "ono",
        		'o', ReactiveIronStack, 'c', fireCore, 'n', ObsidianStack);
        GameRegistry.addRecipe(fireLegsStack, "eie", "ioi", "eie",
        		'o', fireCore, 'e', epDustStack, 'i', ReactiveIronStack);
        GameRegistry.addRecipe(fireBootsStack, "ici", "o o",
        		'o', ObsidianStack, 'i', ReactiveIronStack, 'c', fireCore);
        GameRegistry.addRecipe(iceHelmStack, "ddd", "dcd",
        		'd', ReactiveIronStack, 'c', iceCore);
        GameRegistry.addRecipe(iceChestStack, "o o", "oco", "ooo",
        		'o', ReactiveIronStack, 'c', iceCore);
        GameRegistry.addRecipe(iceLegsStack, "iii", "ioi", "i i",
        		'o', iceCore, 'i', ReactiveIronStack);
        GameRegistry.addRecipe(iceBootsStack, "ici", "i i",
        		'i', ReactiveIronStack, 'c', iceCore);
        GameRegistry.addRecipe(ObsidianStack, "w", "l",
        		'w', waterStack, 'l', lavaStack);
        GameRegistry.addRecipe(cobbleStack, "wl",
        		'w', waterStack, 'l', lavaStack);
        GameRegistry.addRecipe(cobbleStack, "lw",
        		'w', waterStack, 'l', lavaStack);
        GameRegistry.addRecipe(stoneStack, "l", "w",
        		'w', waterStack, 'l', lavaStack);
        GameRegistry.addRecipe(enderPearlStack, "ddd", "ddd", "ddd",
        		'd', epDustStack);
        GameRegistry.addRecipe(newCrusherStack, "fdi", "i i", "ioi",
        		'f', flintStack, 'd', diamondStack, 'i', ironStack, 'o', ObsidianStack);
        GameRegistry.addRecipe(brickHolderStack4, "b b", "brb", "bbb",
        		'b', brickStack, 'r', NetherrackStack);
        GameRegistry.addRecipe(lBurntFleshStack, "xxx", "xxx", "xxx",
        		'x', burntFleshStack);
        GameRegistry.addRecipe(unlogicGemBlockStack, "xxx", "xxx", "xxx",
        		'x', unlogicGem);
        GameRegistry.addRecipe(knowledgeBlockStack, "xxx", "yyy", "xxx",
        		'x', woodStack, 'y', bookshelfStack);
        GameRegistry.addRecipe(endStoneStack, "xx", "xx",
        		'x', endPieceStack);
		GameRegistry.addRecipe(ChocolateCakeStack, "x", "y",
				'x', CocoaBeansStack, 'y', CakeStack);
		GameRegistry.addRecipe(BlazeCakeStack, "x", "y", "z",
				'x', BlazePowderStack, 'y', SlimeballStack, 'z', CakeStack);
		GameRegistry.addRecipe(BlazeCakeStack, "x", "y", "z",
				'y', BlazePowderStack, 'x', SlimeballStack, 'z', CakeStack);
		GameRegistry.addRecipe(BlazeCakeStack, "x", "y", "z",
				'x', BlazePowderStack, 'y', SlimeballStack, 'z', ChocolateCakeStack);
		GameRegistry.addRecipe(BlazeCakeStack, "x", "y", "z",
				'y', BlazePowderStack, 'x', SlimeballStack, 'z', ChocolateCakeStack);
		GameRegistry.addRecipe(BlazeCakeStack, "x", "y",
				'x', MagmaCreamStack, 'y', CakeStack);
		GameRegistry.addRecipe(BlazeCakeStack, "x", "y",
				'x', MagmaCreamStack, 'y', ChocolateCakeStack);
        GameRegistry.addShapelessRecipe(endPieceStack, netherCleanserStack, netherCorruptorStack);
        GameRegistry.addRecipe(NeedleBundleStack, "xxx", "xxx", "xxx",
        		'x', NeedleStack);
        GameRegistry.addRecipe(NeedleStack9, "x",
        		'x', NeedleBundleStack);
        GameRegistry.addRecipe(NeedleBlockStack, "xx", "xx",
        		'x', NeedleBundleStack);
        GameRegistry.addRecipe(NeedleBundleStack4, "x",
        		'x', NeedleBlockStack);
        GameRegistry.addRecipe(WeakTorchStack2, "x", "y",
        		'x', TinyCoalStack, 'y', StickStack);
        GameRegistry.addRecipe(ObsidianToolStack, "dgd", "dsd", " s ",
        		'd', diamondStack, 'g', unlogicGem, 's', StickStack);
        GameRegistry.addShapelessRecipe(CactusStack, StrippedCactusStack, NeedleBundleStack);
        GameRegistry.addShapelessRecipe(StrippedCactusStack, CactusStack, canisterStack);
        GameRegistry.addShapelessRecipe(TinyCoalDustStack8, CoalDustStack);
        GameRegistry.addShapelessRecipe(WeakSlimeStack4, waterStack, SlimeballStack);
        GameRegistry.addShapelessRecipe(TinyCoalStack8, WeakSlimeStack, TinyCoalDustStack, TinyCoalDustStack, TinyCoalDustStack, TinyCoalDustStack, TinyCoalDustStack, TinyCoalDustStack, TinyCoalDustStack, TinyCoalDustStack);
        GameRegistry.addRecipe(GoldBombStack, "ogo", "opo", "oao",
        		'o', ObsidianStack, 'g', GoldStack, 'p', paperStack, 'a', AntiGoldStack);
        //Extended Workbench Recipes
        if(EWAPI.getEWInstalled())
        {
                EWAPI.addRecipe(new ItemStack(Blocks.obsidian, 9), new Object[] {"XXX", "XXX", "XXX", "YYY", "YYY", "YYY", ('X'), Items.water_bucket, ('Y'), Items.lava_bucket});
                EWAPI.addRecipe(new ItemStack(Blocks.stone, 9), new Object[] {"YYY", "YYY", "YYY", "XXX", "XXX", "XXX", ('X'), Items.water_bucket, ('Y'), Items.lava_bucket});
                EWAPI.addRecipe(newCrusherStack, new Object[] {"fif", "fif", "i i", "i i", "ibi", ('f'), Items.flint, ('i'), Items.iron_ingot, ('b'), Blocks.iron_block});
                EWAPI.addRecipe(knowledgeBlockStack, new Object[] {"www", "bbb", "bbb", "bbb", "www", ('w'), Blocks.planks, ('b'), bookStack});
                EWAPI.addRecipe(knowledgeBlockStack, new Object[] {"www", "bbb", "bbb", "bbb", "www", ('w'), Blocks.planks, ('b'), writtenBookStack});
        }
        //Nether Cleanser Recipes
        GameRegistry.addShapelessRecipe(newNetherCleanserStack, netherWartStack, bottleStack, hayStack);
        GameRegistry.addShapelessRecipe(enderPearlStack, netherCleanserStack, magmaCreamStack);
        GameRegistry.addShapelessRecipe(brickStack, netherCleanserStack, netherrackBrickStack);
        GameRegistry.addShapelessRecipe(bricksStack, netherCleanserStack, netherBrickStack);
        GameRegistry.addShapelessRecipe(cobblestoneStack, netherCleanserStack, NetherrackStack);
        GameRegistry.addShapelessRecipe(ironStack, netherCleanserStack, netherQuartzStack);
        GameRegistry.addShapelessRecipe(ironBlockStack, netherCleanserStack, quartzBlockStack);
        GameRegistry.addShapelessRecipe(ironOreStack, netherCleanserStack, netherQuartzOreStack);
        GameRegistry.addShapelessRecipe(fenceStack, netherCleanserStack, nbFenceStack);
        GameRegistry.addShapelessRecipe(brickStairsStack, netherCleanserStack, nbStairsStack);
        //Nether Corrupter Recipes
        GameRegistry.addShapelessRecipe(newNetherCorruptorStack, netherWartStack, bottleStack, coalBlockStack);
        GameRegistry.addShapelessRecipe(magmaCreamStack, netherCorruptorStack, enderPearlStack);
        GameRegistry.addShapelessRecipe(netherrackBrickStack, netherCorruptorStack, brickStack);
        GameRegistry.addShapelessRecipe(netherBrickStack, netherCorruptorStack, bricksStack);
        GameRegistry.addShapelessRecipe(NetherrackStack, netherCorruptorStack, cobblestoneStack);
        GameRegistry.addShapelessRecipe(netherQuartzStack, netherCorruptorStack, ironStack);
        GameRegistry.addShapelessRecipe(quartzBlockStack, netherCorruptorStack, ironBlockStack);
        GameRegistry.addShapelessRecipe(netherQuartzOreStack, netherCorruptorStack, ironOreStack);
        GameRegistry.addShapelessRecipe(nbFenceStack, netherCorruptorStack, fenceStack);
        GameRegistry.addShapelessRecipe(nbStairsStack, netherCorruptorStack, brickStairsStack);
        //Smelting Recipes
        GameRegistry.addSmelting(rottenFleshStack, burntFleshStack, 1);
        GameRegistry.addSmelting(lBurntFleshStack, charcoalStack, 5);
        GameRegistry.addSmelting(burntMeatStack, charcoalStack, 5);
        GameRegistry.addSmelting(chickenStack, burntMeatStack, 2);
        GameRegistry.addSmelting(steakStack, burntMeatStack, 2);
        GameRegistry.addSmelting(fishStack, burntMeatStack, 2);
        GameRegistry.addSmelting(porkStack, burntMeatStack, 2);
        GameRegistry.addSmelting(hayStack, charcoalStack, 2);
        //Crusher Recipes
        //Small flowers yield 2, large yield 4
        GameRegistry.addShapelessRecipe(SilverDyeStack2, crusherStack, smSilverStack);
        GameRegistry.addShapelessRecipe(SilverDyeStack2, crusherStack, smSilver2Stack);
        GameRegistry.addShapelessRecipe(SilverDyeStack2, crusherStack, smSilver3Stack);
        GameRegistry.addShapelessRecipe(RedDyeStack2, crusherStack, smRedStack);
        GameRegistry.addShapelessRecipe(RedDyeStack2, crusherStack, smRed2Stack);
        GameRegistry.addShapelessRecipe(RedDyeStack4, crusherStack, lgRedStack);
        GameRegistry.addShapelessRecipe(YellowDyeStack2, crusherStack, smYellowStack);
        GameRegistry.addShapelessRecipe(YellowDyeStack4, crusherStack, lgYellowStack);
        GameRegistry.addShapelessRecipe(SkyDyeStack2, crusherStack, smSkyStack);
        GameRegistry.addShapelessRecipe(MagentaDyeStack2, crusherStack, smMagentaStack);
        GameRegistry.addShapelessRecipe(MagentaDyeStack4, crusherStack, lgMagentaStack);
        GameRegistry.addShapelessRecipe(PinkDyeStack2, crusherStack, smPinkStack);
        GameRegistry.addShapelessRecipe(PinkDyeStack4, crusherStack, lgPinkStack);
        GameRegistry.addShapelessRecipe(OrangeDyeStack2, crusherStack, smOrangeStack);
        
        GameRegistry.addShapelessRecipe(stringStack2, reedStack, crusherStack);
        GameRegistry.addShapelessRecipe(epDustStack9, enderPearlStack, crusherStack);
        GameRegistry.addShapelessRecipe(bonemealStack4, boneStack, crusherStack);
        GameRegistry.addShapelessRecipe(redstoneStack2, redGemStack, crusherStack);
        GameRegistry.addShapelessRecipe(lapisStack2, blueGemStack, crusherStack);
        GameRegistry.addShapelessRecipe(fSpiderEyeStack2, bMushroomStack, spiderEyeStack, sugarStack, crusherStack);
        GameRegistry.addShapelessRecipe(BlazePowderStack3, crusherStack, blazeRodStack);
        GameRegistry.addShapelessRecipe(CoalDustStack, crusherStack, CoalStack);
        GameRegistry.addShapelessRecipe(TinyCoalDustStack, crusherStack, TinyCoalStack);
        //Canvas Recipes
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack0, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeBlack"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack1, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeRed"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack2, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeGreen"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack3, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeBrown"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack4, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeBlue"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack5, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyePurple"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack5, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeViolet"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack6, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeCyan"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack7, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeSilver"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack7, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeLightGray"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack7, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeLightGrey"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack8, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeGray"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack8, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeGrey"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack9, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyePink"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack10, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeLime"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack11, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeYellow"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack12, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeSky"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack12, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeLightBlue"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack13, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeMagenta"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack14, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeOrange"}));
        CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(CStack15, new Object[]{"yxy", "xwx", "yxy",
        		'w', woolStack, 'x', paperStack, 'y', "dyeWhite"}));
    	//Deperecated content removal recipes
        //Remove in 1.5
    	GameRegistry.addShapelessRecipe(BlackDyeStack9, LStack0);
    	GameRegistry.addShapelessRecipe(RedDyeStack9, LStack1);
    	GameRegistry.addShapelessRecipe(GreenDyeStack9, LStack2);
    	GameRegistry.addShapelessRecipe(BrownDyeStack9, LStack3);
    	GameRegistry.addShapelessRecipe(BlueDyeStack9, LStack4);
    	GameRegistry.addShapelessRecipe(PurpleDyeStack9, LStack5);
    	GameRegistry.addShapelessRecipe(CyanDyeStack9, LStack6);
    	GameRegistry.addShapelessRecipe(SilverDyeStack9, LStack7);
    	GameRegistry.addShapelessRecipe(GreyDyeStack9, LStack8);
    	GameRegistry.addShapelessRecipe(PinkDyeStack9, LStack9);
    	GameRegistry.addShapelessRecipe(LimeDyeStack9, LStack10);
    	GameRegistry.addShapelessRecipe(YellowDyeStack9, LStack11);
    	GameRegistry.addShapelessRecipe(SkyDyeStack9, LStack12);
    	GameRegistry.addShapelessRecipe(MagentaDyeStack9, LStack13);
    	GameRegistry.addShapelessRecipe(OrangeDyeStack9, LStack14);
    	GameRegistry.addShapelessRecipe(BoneMealStack9, LStack15);
    	//Remove in 1.6
        GameRegistry.addRecipe(unlogicGem, "xy", "yx",
        		'x', redGemStack, 'y', blueGemStack);
        GameRegistry.addRecipe(unlogicGem, "yx", "xy",
        		'x', redGemStack, 'y', blueGemStack);
        GameRegistry.addShapelessRecipe(redGemStack9, redGemBlockStack);
        GameRegistry.addShapelessRecipe(blueGemStack9, blueGemBlockStack);
	}
}
