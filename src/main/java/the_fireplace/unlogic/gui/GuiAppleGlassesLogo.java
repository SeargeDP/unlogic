package the_fireplace.unlogic.gui;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;

public class GuiAppleGlassesLogo extends Gui {
	private Minecraft mc;
	private int height;
	private int width;
	public static final ResourceLocation texture = new ResourceLocation("unlogic:textures/gui/apple_glasses_logo.png");
	public static final ResourceLocation texture2 = new ResourceLocation("unlogic:textures/gui/disabled.png");
	public static ResourceLocation texture3 = texture2;
	
	public GuiAppleGlassesLogo(Minecraft minecraft) {
		super();
		this.mc = minecraft;
	}
	@SubscribeEvent(priority = EventPriority.NORMAL)
	public void onRenderGameOverlay(RenderGameOverlayEvent event){
		height = Minecraft.getMinecraft().displayHeight;
		width = Minecraft.getMinecraft().displayWidth;
		//if(event.player.getCurrentArmor(3) == new ItemStack(unbase.AppleGlasses)){
		if(event.type == ElementType.HELMET){//not sure if helmet is correct, but I will use it.
		Tessellator tessellator = Tessellator.instance;
		this.mc.getTextureManager().bindTexture(texture3);
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(0.0D, (double)height, -90.0D, 0.0D, 1.0D);
        tessellator.addVertexWithUV((double)width, (double)height, -90.0D, 1.0D, 1.0D);
        tessellator.addVertexWithUV((double)width, 0.0D, -90.0D, 1.0D, 0.0D);
        tessellator.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
        tessellator.draw();
		}
	}//}
}
