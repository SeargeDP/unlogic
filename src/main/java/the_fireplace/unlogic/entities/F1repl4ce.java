package the_fireplace.unlogic.entities;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.renderers.RenderF1repl4ce;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityBlaze;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class F1repl4ce extends EntityBlaze{
	
    private int heightOffsetUpdateTime;
    private float heightOffset = 0.5F;

	public F1repl4ce(World par1World) {
		super(par1World);
        this.getNavigator().setAvoidsWater(true);
        this.getNavigator().setEnterDoors(true);
		this.tasks.addTask(1, new EntityAISwimming(this));
		this.tasks.addTask(2, new EntityAIAttackOnCollide(this, DJACOB.class, 1.2D, false));
		this.tasks.addTask(3, new EntityAIAttackOnCollide(this, EntityPlayer.class, 1.2D, false));
		this.tasks.addTask(4, new EntityAIWander(this, 1.0D));
		this.tasks.addTask(5, new EntityAIWatchClosest(this, DJACOB.class, 8.0F));
		this.tasks.addTask(6, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
		this.tasks.addTask(7, new EntityAILookIdle(this));

		this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, false));
		this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, DJACOB.class, 0, true));
		this.targetTasks.addTask(3, new EntityAINearestAttackableTarget(this, EntityPlayer.class, 0, true));
        this.isImmuneToFire = true;
        this.experienceValue = 15;
	}
	@Override
	protected void applyEntityAttributes()
	{
		 super.applyEntityAttributes();
	this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(50.0D);
	this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(64.0D);
	this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(0.9D);
	this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.25D);
	this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(10.0D);
	}
	@Override
	public boolean isAIEnabled()
	{
		 return true;
	}
	@Override
    protected String getLivingSound()
    {
        return "mob.blaze.breathe";
    }

	@Override
    protected String getHurtSound()
    {
        return "mob.blaze.hit";
    }

	@Override
    protected String getDeathSound()
    {
        return "mob.blaze.death";
    }
	@Override
    public void attackEntity(Entity par1Entity, float par2) {
		this.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 400));
		this.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 200));
	}
	@Override
	public int getMaxSpawnedInChunk()
    {
        return 1;
    }
	@Override
	protected void dropFewItems(boolean par1, int par2)
    {
        if (par1)
        {
            int j = this.rand.nextInt(3 + par2);

            for (int k = 0; k < j; ++k)
            {
                this.dropItem(unbase.ObsidianRod, 1);
            }
        }
    }
	@Override
	public void onLivingUpdate(){
        if (!this.worldObj.isRemote)
        {
		--this.heightOffsetUpdateTime;

        if (this.heightOffsetUpdateTime <= 0)
        {
            this.heightOffsetUpdateTime = 100;
            this.heightOffset = 0.5F + (float)this.rand.nextGaussian() * 3.0F;
        }

        if (this.getEntityToAttack() != null && this.getEntityToAttack().posY + (double)this.getEntityToAttack().getEyeHeight() > this.posY + (double)this.getEyeHeight() + (double)this.heightOffset)
        {
            this.motionY += (0.30000001192092896D - this.motionY) * 0.30000001192092896D;
        }
    }

    if (this.rand.nextInt(24) == 0)
    {
        this.worldObj.playSoundEffect(this.posX + 0.5D, this.posY + 0.5D, this.posZ + 0.5D, "fire.fire", 1.0F + this.rand.nextFloat(), this.rand.nextFloat() * 0.7F + 0.3F);
    }

    if (!this.onGround && this.motionY < 0.0D)
    {
        this.motionY *= 0.6D;
    }

    for (int i = 0; i < 2; ++i)
    {
        this.worldObj.spawnParticle("flame", this.posX + (this.rand.nextDouble() - 0.5D) * (double)this.width, this.posY + this.rand.nextDouble() * (double)this.height, this.posZ + (this.rand.nextDouble() - 0.5D) * (double)this.width, 0.0D, 0.0D, 0.0D);
    }

    super.onLivingUpdate();
	}
}
