package the_fireplace.unlogic.entities;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAvoidEntity;
import net.minecraft.entity.ai.EntityAIFollowParent;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMate;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAITempt;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.world.World;

public class SnackAttackMan extends EntityCreature{

	public SnackAttackMan(World p_i1595_1_) {
		super(p_i1595_1_);
        this.getNavigator().setAvoidsWater(true);
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.baked_potato, false));
        this.tasks.addTask(1, new EntityAITempt(this, 1.2D, Items.beef, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.bread, false));
        this.tasks.addTask(0, new EntityAITempt(this, 1.2D, Items.cake, false));
        this.tasks.addTask(1, new EntityAITempt(this, 1.2D, Items.chicken, false));
        this.tasks.addTask(1, new EntityAITempt(this, 1.2D, Items.cooked_beef, false));
        this.tasks.addTask(1, new EntityAITempt(this, 1.2D, Items.cooked_chicken, false));
        this.tasks.addTask(1, new EntityAITempt(this, 1.2D, Items.cooked_fished, false));
        this.tasks.addTask(1, new EntityAITempt(this, 1.2D, Items.cooked_porkchop, false));
        this.tasks.addTask(0, new EntityAITempt(this, 1.2D, Items.cookie, false));
        this.tasks.addTask(1, new EntityAITempt(this, 1.2D, Items.fish, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.golden_apple, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.apple, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.golden_carrot, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.melon, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.mushroom_stew, false));
        this.tasks.addTask(1, new EntityAITempt(this, 1.2D, Items.porkchop, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.potato, false));
        this.tasks.addTask(0, new EntityAITempt(this, 1.2D, Items.pumpkin_pie, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.speckled_melon, false));
        this.tasks.addTask(0, new EntityAITempt(this, 1.2D, Items.sugar, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.carrot, false));
        this.tasks.addTask(2, new EntityAITempt(this, 1.2D, Items.carrot_on_a_stick, false));
        this.tasks.addTask(3, new EntityAISwimming(this));
        this.tasks.addTask(4, new EntityAIPanic(this, 1.25D));
        this.tasks.addTask(5, new EntityAIWander(this, 1.0D));
        this.tasks.addTask(6, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(7, new EntityAILookIdle(this));
	}
	@Override
	protected void applyEntityAttributes()
	{
		 super.applyEntityAttributes();
	this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(18.0D);
	this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(32.0D);
	this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.40D);
	}
}
