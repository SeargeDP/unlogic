package the_fireplace.unlogic.worldgen;

import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.WorldChunkManager;

public class UnLogicWorldChunkManager extends WorldChunkManager{
	public UnLogicWorldChunkManager(){
		super();
		this.allowedBiomes.clear();
		this.allowedBiomes.add(BiomeGenBase.hell);
		this.allowedBiomes.add(BiomeGenBase.sky);
		this.allowedBiomes.add(BiomeGenBase.desert);
		this.allowedBiomes.add(BiomeGenBase.extremeHills);
	}

}
