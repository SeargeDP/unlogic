package the_fireplace.unlogic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Random;

import the_fireplace.unlogic.armor.*;
import the_fireplace.unlogic.blocks.*;
import the_fireplace.unlogic.blocks.internal.BlazeCaek;
import the_fireplace.unlogic.blocks.internal.ChocolateCaek;
import the_fireplace.unlogic.blocks.internal.QuartzCrop;
import the_fireplace.unlogic.config.ConfigChangedHandler;
import the_fireplace.unlogic.config.UnLogicConfigValues;
import the_fireplace.unlogic.entities.*;
import the_fireplace.unlogic.gui.GuiAppleGlassesLogo;
import the_fireplace.unlogic.items.*;
import the_fireplace.unlogic.items.components.*;
import the_fireplace.unlogic.items.largedyes.*;
import the_fireplace.unlogic.items.weapons.*;
import the_fireplace.unlogic.items.tools.*;
import the_fireplace.unlogic.proxy.*;
import the_fireplace.unlogic.recipes.UnLogicFuelHandler;
import the_fireplace.unlogic.recipes.VanillaRecipes;
import the_fireplace.unlogic.worldgen.*;
import net.java.games.input.Keyboard;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.command.ICommandSender;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemReed;
import net.minecraft.item.ItemSeeds;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.stats.Achievement;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.AchievementPage;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import the_fireplace.fireplacecore.EWAPI;
import the_fireplace.fireplacecore.FireCoreBaseFile;
import the_fireplace.fireplacecore.config.FCCV;

@Mod(modid=unbase.MODID, name=unbase.MODNAME, version=unbase.VERSION, dependencies="required-after:fireplacecore@[1.0.3.0,)", guiFactory = "the_fireplace.unlogic.config.UnLogicGuiFactory")
public class unbase {
		
        // The instance of your mod that Forge uses.
        @Instance(unbase.MODID)
        public static unbase instance;
        public static final String MODID="unlogic";
        public static final String MODNAME="UnLogic";
        public static final String VERSION="1.0.2.0";
        public static final String DEVKEY="F1repl4ce";

    	private static int updateNotification;
    	private static String releaseVersion;
    	private static String prereleaseVersion;
    	private static final String downloadURL = "http://goo.gl/9yKstR";
    	
    	public static Configuration file;
    	//Config Properties
    	public static Property ENABLEDEBUG_PROPERTY;
    	public static Property ENABLEMOBS_PROPERTY;
    	public static Property DEVKEY_PROPERTY;
    	public static void syncConfig(){
    		UnLogicConfigValues.ENABLEDEBUG = ENABLEDEBUG_PROPERTY.getBoolean();
    		UnLogicConfigValues.DEVKEY = DEVKEY_PROPERTY.getString();
    		UnLogicConfigValues.ENABLEMOBS = ENABLEMOBS_PROPERTY.getBoolean();
    		if(file.hasChanged()){
    	        file.save();
    		}
    	}
    	
        //proxy information
        @SidedProxy(clientSide="the_fireplace.unlogic.proxy.ClientProxy", serverSide="the_fireplace.unlogic.proxy.CommonProxy")
        public static CommonProxy proxy;
        
        private static int primaryColor;
        private static int secondaryColor;
        //set egg colors
        public static void getEggColor(Class entityClass){//RGB to RGB int at http://www.shodor.org/stella2java/rgbint.html
        	if(entityClass == F1repl4ce.class){
        		primaryColor = 13132800;//orange
            	secondaryColor = 1310735;//obsidian purple
        	}else if(entityClass == DJACOB.class){
        		primaryColor = 11513775;//light grey
            	secondaryColor = 8519830;//purple
        	}else if(entityClass == SnackAttackMan.class){
        		primaryColor = 0;//black
        		secondaryColor = 16711680;//red
        	}
        }
        //Entity Registry
    	public static void registerEntity(Class entityClass, String name)
    	{
    	int entityID = EntityRegistry.findGlobalUniqueEntityId();
    	primaryColor = 0;//default black
    	secondaryColor = 16777215;//default white
    	getEggColor(entityClass);

    	EntityRegistry.registerGlobalEntityID(entityClass, name, entityID);
    	EntityRegistry.registerModEntity(entityClass, name, entityID, instance, 64, 1, true);
    	EntityList.entityEggs.put(Integer.valueOf(entityID), new EntityList.EntityEggInfo(entityID, primaryColor, secondaryColor));
    	}
    	public static void registerEntityWithoutEgg(Class entityClass, String name)
    	{
    	int entityID = EntityRegistry.findGlobalUniqueEntityId();
    	EntityRegistry.registerGlobalEntityID(entityClass, name, entityID);
    	EntityRegistry.registerModEntity(entityClass, name, entityID, instance, 32, 1, true);
    	}
        //Creative Tab Registry
        public static CreativeTabs TabUnLogic = new TabUnLogic("UnLogic");
        //Armor Materials
        public static ArmorMaterial armorFire = EnumHelper.addArmorMaterial("FIRE", 20, new int[]{5, 7, 2, 4}, 50);
        public static ArmorMaterial armorIce = EnumHelper.addArmorMaterial("ICE", 40, new int[]{4, 5, 5, 3}, 30);
        public static ArmorMaterial accessory = EnumHelper.addArmorMaterial("ACCESSORY", 5, new int[]{0,0,0,0}, 5);
        //Tool Materials
        public static ToolMaterial reactive = EnumHelper.addToolMaterial("REACTIVE", 2, 250, 6.0F, 2.0F, 45);
        //Block Registry
        public static Block QCrop = new QuartzCrop();
        public static Block BoneOre = new BoneOre(Material.rock).setCreativeTab(TabUnLogic);
        public static Block BrickFireHolder = new BrickFireHolder(Material.rock).setCreativeTab(TabUnLogic);
        public static Block KnowledgeBlock = new KnowledgeBlock(Material.wood).setCreativeTab(TabUnLogic);
        public static Block FarmObsidian = new FarmObsidian(Material.rock)/*.setCreativeTab(TabUnLogic)*/;
        public static Block UnLogicGemBlock = new UnLogicGemBlock(Material.circuits).setCreativeTab(TabUnLogic);
        public static Block UnLogicStone = new SecondStone(Material.rock)/*.setCreativeTab(TabUnLogic)*/;
        public static Block BlackScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_black").setBlockName("BlackScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block BlueScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_blue").setBlockName("BlueScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block BrownScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_brown").setBlockName("BrownScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block CyanScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_cyan").setBlockName("CyanScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block GreenScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_green").setBlockName("GreenScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block LimeScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_lime").setBlockName("LimeScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block MagentaScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_magenta").setBlockName("MagentaScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block OrangeScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_orange").setBlockName("OrangeScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block PinkScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_pink").setBlockName("PinkScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block PurpleScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_purple").setBlockName("PurpleScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block RedScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_red").setBlockName("RedScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block SilverScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_silver").setBlockName("SilverScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block SkyScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_sky").setBlockName("SkyScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block WhiteScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_white").setBlockName("WhiteScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block YellowScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_yellow").setBlockName("YellowScreen").setCreativeTab(unbase.TabUnLogic);
        public static Block GreyScreen = new Screen(Material.cloth).setBlockTextureName("unlogic:wool_colored_grey").setBlockName("GreyScreen").setCreativeTab(unbase.TabUnLogic);
    	public static Block ChocolateCake = new ChocolateCaek(Material.cake).setBlockName("ChocolateCaek");
    	public static Block BlazeCake = new BlazeCaek(Material.cake).setBlockName("BlazeCaek");
    	public static Block MSTable = new MSTable(Material.wood).setBlockName("MSTable")/*.setCreativeTab(TabUnLogic)*/;
    	public static Block PortalFrame = new PortalFrameBlock(Material.iron).setBlockName("UnLogicPortalFrame");
    	public static Block StrippedCactus = new StrippedCactus().setBlockName("StrippedCactus").setCreativeTab(TabUnLogic);
    	public static Block NeedleBlock = new NeedleBlock(Material.iron).setBlockTextureName("unlogic:NeedleBlock").setBlockName("UnLogicNeedleBlock").setCreativeTab(TabUnLogic);
    	public static Block WeakTorch = new WeakTorch().setLightLevel(0.7F);
    	public static Block LightConducter = new LightConducter(Material.iron).setBlockTextureName("unlogic:LightConducter").setBlockName("LightConducter")/*.setCreativeTab(TabUnLogic)*/;
    	public static Block DepletionFurnace = new DepletionFurnace(false).setBlockName("DepletionFurnace")/*.setCreativeTab(TabUnLogic)*/;
    	public static Block DepletionFurnaceOn = new DepletionFurnace(true).setBlockName("DepletionFurnaceOn");
    	public static Block GoldBomb = new GoldBomb(Material.tnt);
        //Item Registry
        public static Item QSeed = new ItemSeeds(QCrop, FarmObsidian).setUnlocalizedName("QSeed").setTextureName("unlogic:QSeeds").setCreativeTab(TabUnLogic);
        public static Item FireHelmet = new FireArmor(armorFire, 0, 0).setUnlocalizedName("FireHelmet").setCreativeTab(TabUnLogic).setTextureName("unlogic:FireHelmet");
        public static Item FireChestplate = new FireArmor(armorFire, 0, 1).setUnlocalizedName("FireChestplate").setCreativeTab(TabUnLogic).setTextureName("unlogic:FireChestplate");
        public static Item FireLeggings = new FireArmor(armorFire, 0, 2).setUnlocalizedName("FireLeggings").setCreativeTab(TabUnLogic).setTextureName("unlogic:FireLeggings");
        public static Item FireBoots = new FireArmor(armorFire, 0, 3).setUnlocalizedName("FireBoots").setCreativeTab(TabUnLogic).setTextureName("unlogic:FireBoots");
        public static Item IceHelmet = new IceArmor(armorIce, 0, 0).setUnlocalizedName("IceHelmet").setCreativeTab(TabUnLogic).setTextureName("unlogic:ice_helmet");
        public static Item IceChestplate = new IceArmor(armorIce, 0, 1).setUnlocalizedName("IceChestplate").setCreativeTab(TabUnLogic).setTextureName("unlogic:ice_chestplate");
        public static Item IceLeggings = new IceArmor(armorIce, 0, 2).setUnlocalizedName("IceLeggings").setCreativeTab(TabUnLogic).setTextureName("unlogic:ice_leggings");
        public static Item IceBoots = new IceArmor(armorIce, 0, 3).setUnlocalizedName("IceBoots").setCreativeTab(TabUnLogic).setTextureName("unlogic:ice_boots");
        public static Item HandheldCrusher = new HandheldCrusher().setUnlocalizedName("HandheldCrusher").setCreativeTab(TabUnLogic);
        public static Item UnlogicGem = new UnlogicGem(-20, true).setTextureName("unlogic:unlogicGem");
        public static Item AntiGold = new AntiGold().setTextureName("unlogic:antiGold");
        public static Item BurntFlesh = new BurntFlesh().setTextureName("unlogic:burntflesh");
        public static Item BurntMeat = new BurntMeat().setTextureName("unlogic:meatburnt");
        public static Item Canister = new FermentedSpiderEyeCanister().setTextureName("unlogic:canister");
        public static Item EnderPearlDust = new EnderPearlDust().setTextureName("unlogic:epDust");
        public static Item FireCore = new FireCore().setTextureName("unlogic:fireCore");
        public static Item IceCore = new IceCore().setTextureName("unlogic:iceCore");
        public static Item LargeBurntFlesh = new LargeBurntFlesh().setTextureName("unlogic:largeburntflesh");
        public static Item NetherCleanser = new NetherCleanser();
        public static Item NetherCorruptor = new NetherCorruptor();
        public static Item EndPiece = new EndPiece();
    	public static Item ItemChocolateCake = new ItemReed(ChocolateCake).setMaxStackSize(1).setCreativeTab(TabUnLogic).setUnlocalizedName("ChocolateCaekItem").setTextureName("unlogic:chocolate");
    	public static Item ItemBlazeCake = new ItemReed(BlazeCake).setMaxStackSize(1).setCreativeTab(TabUnLogic).setUnlocalizedName("BlazeCaekItem").setTextureName("unlogic:blaze");
    	public static Item Needle = new Needle().setUnlocalizedName("UnLogicNeedle");
    	public static Item NeedleBundle = new NeedleBundle().setUnlocalizedName("UnLogicNeedleBundle").setTextureName("unlogic:NeedleBundle").setCreativeTab(TabUnLogic);
    	public static Item CoalDust = new CoalDust();
    	public static Item TinyCoalDust = new TinyCoalDust();
    	public static Item TinyCoal = new TinyCoal();
    	public static Item WeakSlime = new WeakSlime();
    	public static Item ObsidianRod = new ObsidianRod();
    	public static Item Wabbajack = new Wabbajack();
    	public static Item ReactiveIron = new ReactiveIron();
    	public static Item AppleGlasses = new AppleGlasses(accessory, 0, 0).setUnlocalizedName("AppleGlasses")/*.setCreativeTab(TabUnLogic)*/.setTextureName("unlogic:AppleGlasses");
    	public static Item FireBrand = new FireBrand(reactive);
    	public static Item IceBrand = new IceBrand(reactive);
    	public static Item ReactiveSword = new ReactiveIronSword(reactive);
    	public static Item ObsidianTool = new ObsidianTool();
    	//Deprecated content
    	//Remove in 1.1.0.0
    		public static Item LBlackDye = new Black();
    		public static Item LRedDye = new Red();
    		public static Item LGreenDye = new Green();
    		public static Item LYellowDye = new Yellow();
    		public static Item LBlueDye = new Blue();
    		public static Item LLimeDye = new Lime();
    		public static Item LSkyDye = new Sky();
    		public static Item LSilverDye = new Silver();
    		public static Item LGreyDye = new Grey();
    		public static Item LWhiteDye = new MealWhite();
    		public static Item LTrueWhiteDye = new White();
        	public static Item LOrangeDye = new Orange();
        	public static Item LMagentaDye = new Magenta();
        	public static Item LPinkDye = new Pink();
        	public static Item LBrownDye = new Brown();
        	public static Item LPurpleDye = new Purple();
        	public static Item LCyanDye = new Cyan();
        //Remove in 1.2.0.0
            public static Item RedGem = new RedCrystal().setTextureName("unlogic:redGem");
            public static Item BlueGem = new BlueCrystal().setTextureName("unlogic:blueGem");
            public static Block RedGemBlock = new RedGemBlock(Material.rock);
            public static Block BlueGemBlock = new BlueGemBlock(Material.rock);
        
        @EventHandler
        public void preInit(FMLPreInitializationEvent event) {
        	FMLCommonHandler.instance().bus().register(new ConfigChangedHandler());
    		file = new Configuration(event.getSuggestedConfigurationFile());
    		file.load();
    		ENABLEDEBUG_PROPERTY = file.get(Configuration.CATEGORY_GENERAL, UnLogicConfigValues.ENABLEDEBUG_NAME, UnLogicConfigValues.ENABLEDEBUG_DEFAULT);
    		DEVKEY_PROPERTY = file.get(Configuration.CATEGORY_GENERAL, UnLogicConfigValues.DEVKEY_NAME, UnLogicConfigValues.DEVKEY_DEFAULT);
    		ENABLEMOBS_PROPERTY = file.get(Configuration.CATEGORY_GENERAL, UnLogicConfigValues.ENABLEMOBS_NAME, UnLogicConfigValues.ENABLEMOBS_DEFAULT);
    		syncConfig();
            //Item/Block Registry
            GameRegistry.registerBlock(QCrop, "Quartz Crop");
            GameRegistry.registerBlock(BoneOre, "UnLogic Bone Ore");
            GameRegistry.registerBlock(BrickFireHolder, "Brick Fire Holder");
            GameRegistry.registerBlock(KnowledgeBlock, "Compressed Knowledge Block");
            GameRegistry.registerBlock(FarmObsidian, "Farm Obsidian");
            GameRegistry.registerBlock(UnLogicGemBlock, "UnLogic Gem Block");
            GameRegistry.registerBlock(UnLogicStone, "UnLogicStone");
            GameRegistry.registerBlock(BlackScreen, "Black Screen");
            GameRegistry.registerBlock(BlueScreen, "Blue Screen");
            GameRegistry.registerBlock(BrownScreen, "Brown Screen");
            GameRegistry.registerBlock(CyanScreen, "Cyan Screen");
            GameRegistry.registerBlock(GreenScreen, "Green Screen");
            GameRegistry.registerBlock(GreyScreen, "Grey Screen");
            GameRegistry.registerBlock(LimeScreen, "Lime Screen");
            GameRegistry.registerBlock(MagentaScreen, "Magenta Screen");
            GameRegistry.registerBlock(OrangeScreen, "Orange Screen");
            GameRegistry.registerBlock(PinkScreen, "Pink Screen");
            GameRegistry.registerBlock(PurpleScreen, "Purple Screen");
            GameRegistry.registerBlock(RedScreen, "Red Screen");
            GameRegistry.registerBlock(SilverScreen, "Silver Screen");
            GameRegistry.registerBlock(SkyScreen, "Sky Screen");
            GameRegistry.registerBlock(WhiteScreen, "White Screen");
            GameRegistry.registerBlock(YellowScreen, "Yellow Screen");
    		GameRegistry.registerBlock(ChocolateCake, "Chocolate Caek");
    		GameRegistry.registerBlock(BlazeCake, "Blaze Caek");
    		GameRegistry.registerBlock(MSTable, "MSTable");
    		GameRegistry.registerBlock(PortalFrame, "UnLogicPortalFrame");
    		GameRegistry.registerBlock(StrippedCactus, "StrippedCactus");
    		GameRegistry.registerBlock(NeedleBlock, "UnLogicNeedleBlock");
    		GameRegistry.registerBlock(WeakTorch, "WeakTorch");
    		GameRegistry.registerBlock(LightConducter, "LightConducter");
    		GameRegistry.registerBlock(DepletionFurnace, "DepletionFurnace");
    		GameRegistry.registerBlock(DepletionFurnaceOn, "DepletionFurnaceOn");
    		GameRegistry.registerBlock(GoldBomb, "GoldBomb");
    		GameRegistry.registerItem(ItemChocolateCake, "Chocolate Caek Item");
    		GameRegistry.registerItem(ItemBlazeCake, "Blaze Caek Item");
            GameRegistry.registerItem(QSeed, "Qseed");
            GameRegistry.registerItem(FireHelmet, "FireHelmet");
            GameRegistry.registerItem(FireChestplate, "FireChestplate");
            GameRegistry.registerItem(FireLeggings, "FireLeggings");
            GameRegistry.registerItem(FireBoots, "FireBoots");
            GameRegistry.registerItem(IceHelmet, "IceHelmet");
            GameRegistry.registerItem(IceChestplate, "IceChestplate");
            GameRegistry.registerItem(IceLeggings, "IceLeggings");
            GameRegistry.registerItem(IceBoots, "IceBoots");
            GameRegistry.registerItem(HandheldCrusher, "Handheld Crusher");
            GameRegistry.registerItem(AntiGold, "AntiGold");
            GameRegistry.registerItem(BurntFlesh, "Burnt Flesh");
            GameRegistry.registerItem(LargeBurntFlesh, "Large Burnt Flesh");
            GameRegistry.registerItem(BurntMeat, "Burnt Meat");
            GameRegistry.registerItem(Canister, "Canister");
            GameRegistry.registerItem(EnderPearlDust, "UnLogic Ender Pearl Dust");
            GameRegistry.registerItem(FireCore, "Fire Core");
            GameRegistry.registerItem(IceCore, "Ice Core");
            GameRegistry.registerItem(UnlogicGem, "UnLogic Gem");
            GameRegistry.registerItem(NetherCleanser, "NetherCleanser");
            GameRegistry.registerItem(NetherCorruptor, "NetherCorruptor");
            GameRegistry.registerItem(EndPiece, "EndPiece");
            GameRegistry.registerItem(Needle, "UnLogicNeedle");
            GameRegistry.registerItem(NeedleBundle, "UnLogicNeedleBundle");
            GameRegistry.registerItem(TinyCoal, "TinyCoal");
            GameRegistry.registerItem(TinyCoalDust, "TinyCoalDust");
            GameRegistry.registerItem(CoalDust, "UnLogicCoalDust");
            GameRegistry.registerItem(WeakSlime, "WeakSlime");
            GameRegistry.registerItem(ObsidianRod, "Obsidian Rod");
            GameRegistry.registerItem(Wabbajack, "Wabbajack");
            GameRegistry.registerItem(ReactiveIron, "Reactive Iron");
            GameRegistry.registerItem(AppleGlasses, "AppleGlasses");
            GameRegistry.registerItem(ReactiveSword, "ReactiveIronSword");
            GameRegistry.registerItem(FireBrand, "FireBrand");
            GameRegistry.registerItem(IceBrand, "IceBrand");
            GameRegistry.registerItem(ObsidianTool, "obsidian_tool");
            //Deprecated Content
            //Remove in 1.1.0.0
            	GameRegistry.registerItem(LBlackDye, "Large Black Dye");
            	GameRegistry.registerItem(LRedDye, "Large Red Dye");
            	GameRegistry.registerItem(LOrangeDye, "Large Orange Dye");
            	GameRegistry.registerItem(LYellowDye, "Large Yellow Dye");
            	GameRegistry.registerItem(LGreenDye, "Large Green Dye");
            	GameRegistry.registerItem(LBlueDye, "Large Blue Dye");
            	GameRegistry.registerItem(LPurpleDye, "Large Purple Dye");
            	GameRegistry.registerItem(LMagentaDye, "Large Magenta Dye");
            	GameRegistry.registerItem(LWhiteDye, "Large White Dye");
            	GameRegistry.registerItem(LTrueWhiteDye, "Large True White Dye");
            	GameRegistry.registerItem(LPinkDye, "Large Pink Dye");
            	GameRegistry.registerItem(LLimeDye, "Large Lime Dye");
            	GameRegistry.registerItem(LGreyDye, "Large Grey Dye");
            	GameRegistry.registerItem(LSkyDye, "Large Sky Dye");
            	GameRegistry.registerItem(LSilverDye, "Large Silver Dye");
            	GameRegistry.registerItem(LCyanDye, "Large Cyan Dye");
            	GameRegistry.registerItem(LBrownDye, "Large Brown Dye");
            //Remove in 1.2.0.0
                GameRegistry.registerItem(BlueGem, "Blue Crystal");
                GameRegistry.registerItem(RedGem, "Red Crystal");
                GameRegistry.registerBlock(RedGemBlock, "UnLogic Red Gem Block");
                GameRegistry.registerBlock(BlueGemBlock, "UnLogic Blue Gem Block");
            
        	//Entity Registry
        	registerEntity(F1repl4ce.class, "FireSoul");
        	registerEntity(DJACOB.class, "DJACOB");
        	if(UnLogicConfigValues.ENABLEDEBUG == true){
        	registerEntity(SnackAttackMan.class, "SnackAttackMan");
        	}else{
            	registerEntityWithoutEgg(SnackAttackMan.class, "SnackAttackMan");
        	}
        	registerEntityWithoutEgg(EntityEnderPearlDust.class, "EnderPearlDust");
        	registerEntityWithoutEgg(EntityUnlogicGem.class, "UnLogicGem");
        	if(UnLogicConfigValues.ENABLEMOBS == true){
        	EntityRegistry.addSpawn(F1repl4ce.class, 1, 0, 1, EnumCreatureType.ambient, BiomeGenBase.hell);
        	EntityRegistry.addSpawn(DJACOB.class, 1, 0, 1, EnumCreatureType.ambient, BiomeGenBase.hell);
        	}
            //Ore Dictionary
            OreDictionary.registerOre("dustEnderPearl", EnderPearlDust);
            OreDictionary.registerOre("dustEnder", EnderPearlDust);
            OreDictionary.registerOre("oreBone", BoneOre);
            OreDictionary.registerOre("needle", Needle);
            OreDictionary.registerOre("dustCoal", CoalDust);
            OreDictionary.registerOre("tinyCoalDust", TinyCoalDust);
            OreDictionary.registerOre("tinyDustCoal", TinyCoalDust);
            OreDictionary.registerOre("dustCoalSmall", TinyCoalDust);
            OreDictionary.registerOre("smallDustCoal", TinyCoalDust);
            //Proxy
            proxy.registerRenderers();
    		//Fuel Handler
            GameRegistry.registerFuelHandler(new UnLogicFuelHandler());
    		retriveCurrentVersions();
        }
        
        @EventHandler
        public void load(FMLInitializationEvent event) {
        	VanillaRecipes.addRecipes();
        	FMLCommonHandler.instance().bus().register(new ConfigChangedHandler());
        	proxy.registerGUIs();
        	//Ore Gen Registry
            GameRegistry.registerWorldGenerator(new WorldGeneratorBoneOre(), 12);
            GameRegistry.registerWorldGenerator(new WorldGeneratorSecondStone(), 1);
            
            ItemStack newCrusherStack = new ItemStack(HandheldCrusher);
            ItemStack burntMeatStack = new ItemStack(BurntMeat);
            ItemStack netherCorruptorStack = new ItemStack(NetherCorruptor);
            ItemStack netherCleanserStack = new ItemStack(NetherCleanser);
            ItemStack fireHelmStack = new ItemStack(FireHelmet);
            ItemStack fireChestStack = new ItemStack(FireChestplate);
            ItemStack fireBootsStack = new ItemStack(FireBoots);
            ItemStack fireLegsStack = new ItemStack(FireLeggings);
            ItemStack coalDustStack = new ItemStack(CoalDust);
            //Chest Loot Gen
            ChestGenHooks.getInfo(ChestGenHooks.BONUS_CHEST).addItem(new WeightedRandomChestContent(newCrusherStack, 1, 1, 4));
            ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_DESERT_CHEST).addItem(new WeightedRandomChestContent(fireHelmStack, 1, 1, 3));
            ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_DESERT_CHEST).addItem(new WeightedRandomChestContent(fireChestStack, 1, 1, 3));
            ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_DESERT_CHEST).addItem(new WeightedRandomChestContent(fireLegsStack, 1, 1, 3));
            ChestGenHooks.getInfo(ChestGenHooks.PYRAMID_DESERT_CHEST).addItem(new WeightedRandomChestContent(fireBootsStack, 1, 1, 3));
            ChestGenHooks.getInfo(ChestGenHooks.DUNGEON_CHEST).addItem(new WeightedRandomChestContent(burntMeatStack, 3, 28, 52));
            ChestGenHooks.getInfo(ChestGenHooks.MINESHAFT_CORRIDOR).addItem(new WeightedRandomChestContent(coalDustStack, 1, 6, 7));
            ChestGenHooks.getInfo(ChestGenHooks.STRONGHOLD_CROSSING).addItem(new WeightedRandomChestContent(netherCorruptorStack, 1, 1, 40));
            ChestGenHooks.getInfo(ChestGenHooks.STRONGHOLD_CROSSING).addItem(new WeightedRandomChestContent(netherCleanserStack, 1, 1, 40));
            ChestGenHooks.getInfo(ChestGenHooks.STRONGHOLD_LIBRARY).addItem(new WeightedRandomChestContent(fireHelmStack, 1, 1, 4));
            ChestGenHooks.getInfo(ChestGenHooks.STRONGHOLD_LIBRARY).addItem(new WeightedRandomChestContent(fireChestStack, 1, 1, 4));
            ChestGenHooks.getInfo(ChestGenHooks.STRONGHOLD_LIBRARY).addItem(new WeightedRandomChestContent(fireLegsStack, 1, 1, 4));
            ChestGenHooks.getInfo(ChestGenHooks.STRONGHOLD_LIBRARY).addItem(new WeightedRandomChestContent(fireBootsStack, 1, 1, 4));
        }
        @SideOnly(Side.CLIENT)
		public static void registerClientInfo(){
        	MinecraftForge.EVENT_BUS.register(new GuiAppleGlassesLogo(Minecraft.getMinecraft()));
        }
        /**
    	 * This method is client side called when a player joins the game. Both for
    	 * a server or a single player world.
    	 */
    	public static void onPlayerJoinClient(EntityPlayer player,
    			ClientConnectedToServerEvent event) {
    		updateNotification = FireCoreBaseFile.getUpdateNotification();
    		if (!prereleaseVersion.equals("")
    				&& !releaseVersion.equals("")) {
    			switch (updateNotification) {
    			case 0:
    				if (isHigherVersion(VERSION, releaseVersion) && isHigherVersion(prereleaseVersion, releaseVersion)) {
    					sendToPlayer(
    							player,
    							"�6A new version of "+MODNAME+" is available!\n�l�c========== �4"
    									+ releaseVersion
    									+ "�c ==========\n"
    									+ "�6Download it at �e" + downloadURL + "�6!");
    				}else if(isHigherVersion(VERSION, prereleaseVersion)){
    					sendToPlayer(
    							player,
    							"�6A new version of "+MODNAME+" is available!\n�l�c========== �4"
    									+ prereleaseVersion
    									+ "�c ==========\n"
    									+ "�6Download it at �e" + downloadURL + "�6!");
    				}

    				break;
    			case 1:
    				if (isHigherVersion(VERSION, releaseVersion)) {
    					sendToPlayer(
    							player,
    							"�6A new version of "+MODNAME+" is available!\n�l�c========== �4"
    									+ releaseVersion
    									+ "�c ==========\n"
    									+ "�6Download it at �e" + downloadURL + "�6!");
    				}
    				break;
    			case 2:
    				
    				break;
    			}
    		}
    	}
    	
    	/**
    	 * Sends a chat message to the current player. Only works client side
    	 * 
    	 * @param message
    	 *            the message to be sent
    	 */
    	private static void sendToPlayer(EntityPlayer player, String message) {
    		String[] lines = message.split("\n");

    		for (String line : lines)
    			((ICommandSender) player)
    					.addChatMessage(new ChatComponentText(line));
    	}

    	/**
    	 * Checks if the new version is higher than the current one
    	 * 
    	 * @param currentVersion
    	 *            The version which is considered current
    	 * @param newVersion
    	 *            The version which is considered new
    	 * @return Whether the new version is higher than the current one or not
    	 */
    	private static boolean isHigherVersion(String currentVersion,
    			String newVersion) {
    		final int[] _current = splitVersion(currentVersion);
    		final int[] _new = splitVersion(newVersion);

    		return (_current[0] < _new[0])
    				|| ((_current[0] == _new[0]) && (_current[1] < _new[1]))
    				|| ((_current[0] == _new[0]) && (_current[1] == _new[1]) && (_current[2] < _new[2]))
    				|| ((_current[0] == _new[0]) && (_current[1] == _new[1]) && (_current[2] == _new[2]) && (_current[3] < _new[3]));
    	}

    	/**
    	 * Splits a version in its number components (Format ".\d+\.\d+\.\d+.*" )
    	 * 
    	 * @param Version
    	 *            The version to be splitted (Format is important!
    	 * @return The numeric version components as an integer array
    	 */
    	private static int[] splitVersion(String Version) {
    		final String[] tmp = Version/*.substring(1)*/.split("\\.");
    		final int size = tmp.length;
    		final int out[] = new int[size];

    		for (int i = 0; i < size; i++) {
    			out[i] = Integer.parseInt(tmp[i]);
    		}

    		return out;
    	}

    	/**
    	 * Retrieves what the latest version is from Dropbox
    	 */
    	private static void retriveCurrentVersions() {
    		try {
    			releaseVersion = get_content(new URL(
    					"https://dl.dropboxusercontent.com/s/of2srr5ul6cjblf/release.version?dl=0")
    					.openConnection());

    			prereleaseVersion = get_content(new URL(
    					"https://dl.dropboxusercontent.com/s/j9vupk07fjnjtb6/prerelease.version?dl=0")
    					.openConnection());

    		} catch (final MalformedURLException e) {
    			System.out.println("Malformed URL Exception");
    			releaseVersion = "";
    			prereleaseVersion = "";
    		} catch (final IOException e) {
    			System.out.println("IO Exception");
    			releaseVersion = "";
    			prereleaseVersion = "";
    		}
    	}

    	private static String get_content(URLConnection con) throws IOException {
    		String output = "";

    		if (con != null) {
    			System.out.println("Getting Content...");
    			final BufferedReader br = new BufferedReader(new InputStreamReader(
    					con.getInputStream()));

    			String input;

    			while ((input = br.readLine()) != null) {
    				output = output + input;
    			}
    			br.close();
    		}

    		return output;
    	}
}