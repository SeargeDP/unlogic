package the_fireplace.unlogic.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelFatMan extends ModelBase
{
  //fields
    ModelRenderer leg2;
    ModelRenderer leg1;
    ModelRenderer Body;
    ModelRenderer head;
    ModelRenderer Arm1;
    ModelRenderer Arm2;
    ModelRenderer SmallFatLayer;
    ModelRenderer FatLayer;
  
  public ModelFatMan()
  {
    textureWidth = 64;
    textureHeight = 64;
    
      leg2 = new ModelRenderer(this, 48, 17);
      leg2.addBox(-2F, -4F, -2F, 4, 12, 4);
      leg2.setRotationPoint(4F, 16F, 2F);
      leg2.setTextureSize(64, 32);
      leg2.mirror = true;
      setRotation(leg2, 0F, 0F, 0F);
      leg1 = new ModelRenderer(this, 0, 0);
      leg1.addBox(-2F, -4F, -2F, 4, 12, 4);
      leg1.setRotationPoint(-4F, 16F, 2F);
      leg1.setTextureSize(64, 32);
      leg1.mirror = true;
      setRotation(leg1, 0F, 0F, 0F);
      Body = new ModelRenderer(this, 0, 26);
      Body.addBox(-6F, -4F, -2F, 12, 16, 5);
      Body.setRotationPoint(0F, 0F, 1F);
      Body.setTextureSize(64, 32);
      Body.mirror = true;
      setRotation(Body, 0F, 0F, 0F);
      head = new ModelRenderer(this, 0, 48);
      head.addBox(-4F, -12F, -5F, 8, 8, 8);
      head.setRotationPoint(0F, 0F, 2F);
      head.setTextureSize(64, 32);
      head.mirror = true;
      setRotation(head, 0F, 0F, 0F);
      Arm1 = new ModelRenderer(this, 31, 0);
      Arm1.addBox(0F, -5F, -2F, 4, 12, 4);
      Arm1.setRotationPoint(6F, 1F, 2F);
      Arm1.setTextureSize(64, 32);
      Arm1.mirror = true;
      setRotation(Arm1, 0F, 0F, 0F);
      Arm2 = new ModelRenderer(this, 48, 0);
      Arm2.addBox(-4F, -5F, -2F, 4, 12, 4);
      Arm2.setRotationPoint(-6F, 1F, 2F);
      Arm2.setTextureSize(64, 32);
      Arm2.mirror = true;
      setRotation(Arm2, 0F, 0F, 0F);
      SmallFatLayer = new ModelRenderer(this, 37, 52);
      SmallFatLayer.addBox(0F, -4F, 0F, 12, 1, 1);
      SmallFatLayer.setRotationPoint(-6F, 8F, -2F);
      SmallFatLayer.setTextureSize(64, 32);
      SmallFatLayer.mirror = true;
      setRotation(SmallFatLayer, 0F, 0F, 0F);
      FatLayer = new ModelRenderer(this, 36, 55);
      FatLayer.addBox(0F, -4F, 0F, 12, 7, 2);
      FatLayer.setRotationPoint(-6F, 9F, -3F);
      FatLayer.setTextureSize(64, 32);
      FatLayer.mirror = true;
      setRotation(FatLayer, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(entity, f, f1, f2, f3, f4, f5);
    leg2.render(f5);
    leg1.render(f5);
    Body.render(f5);
    head.render(f5);
    Arm1.render(f5);
    Arm2.render(f5);
    SmallFatLayer.render(f5);
    FatLayer.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
