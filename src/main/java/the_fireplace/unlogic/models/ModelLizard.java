package the_fireplace.unlogic.models;

import java.util.HashMap;

import the_fireplace.fireplacecore.MCAClientLibrary.MCAModelRenderer;
import the_fireplace.fireplacecore.MCACommonLibrary.MCAVersionChecker;
import the_fireplace.fireplacecore.MCACommonLibrary.animation.AnimationHandler;
import the_fireplace.fireplacecore.MCACommonLibrary.math.Matrix4f;
import the_fireplace.fireplacecore.MCACommonLibrary.math.Quaternion;
import the_fireplace.unlogic.entities.EntityLizard;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

public class ModelLizard extends ModelBase {
public final int MCA_MIN_REQUESTED_VERSION = 1;
public HashMap<String, MCAModelRenderer> parts = new HashMap<String, MCAModelRenderer>();

MCAModelRenderer body;
MCAModelRenderer leg3;
MCAModelRenderer leg2;
MCAModelRenderer leg1;
MCAModelRenderer head;
MCAModelRenderer leg4;
MCAModelRenderer tailBase;
MCAModelRenderer tailMid;
MCAModelRenderer tailTip;
public ModelLizard()
{
MCAVersionChecker.checkForLibraryVersion(getClass(), MCA_MIN_REQUESTED_VERSION);

textureWidth = 128;
textureHeight = 128;

body = new MCAModelRenderer(this, "Body", 0, 0);
body.mirror = false;
body.addBox(0.0F, 0.0F, 0.0F, 16, 4, 8);
body.setInitialRotationPoint(0.0F, 0.0F, 0.0F);
body.setInitialRotationMatrix(new Matrix4f().set(new Quaternion(0.0F, 0.0F, 0.0F, 1.0F)).transpose());
body.setTextureSize(128, 128);
parts.put(body.boxName, body);

leg3 = new MCAModelRenderer(this, "Leg3", 0, 0);
leg3.mirror = false;
leg3.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
leg3.setInitialRotationPoint(2.0F, 2.0F, 8.0F);
leg3.setInitialRotationMatrix(new Matrix4f().set(new Quaternion(-5.7054814E-9F, 0.1305262F, 0.9914449F, -4.3337433E-8F)).transpose());
leg3.setTextureSize(128, 128);
parts.put(leg3.boxName, leg3);
body.addChild(leg3);

leg2 = new MCAModelRenderer(this, "Leg2", 30, 13);
leg2.mirror = false;
leg2.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
leg2.setInitialRotationPoint(14.0F, 2.0F, -1.0F);
leg2.setInitialRotationMatrix(new Matrix4f().set(new Quaternion(5.7054814E-9F, -0.1305262F, 0.9914449F, -4.3337433E-8F)).transpose());
leg2.setTextureSize(128, 128);
parts.put(leg2.boxName, leg2);
body.addChild(leg2);

leg1 = new MCAModelRenderer(this, "Leg1", 24, 15);
leg1.mirror = false;
leg1.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
leg1.setInitialRotationPoint(2.0F, 2.0F, -1.0F);
leg1.setInitialRotationMatrix(new Matrix4f().set(new Quaternion(5.7054814E-9F, -0.1305262F, 0.9914449F, -4.3337433E-8F)).transpose());
leg1.setTextureSize(128, 128);
parts.put(leg1.boxName, leg1);
body.addChild(leg1);

head = new MCAModelRenderer(this, "Head", 57, 0);
head.mirror = false;
head.addBox(-2.0F, 2.0F, 2.0F, 4, 4, 4);
head.setInitialRotationPoint(0.0F, 0.0F, 0.0F);
head.setInitialRotationMatrix(new Matrix4f().set(new Quaternion(0.0F, 0.0F, 0.0F, 1.0F)).transpose());
head.setTextureSize(128, 128);
parts.put(head.boxName, head);
body.addChild(head);

leg4 = new MCAModelRenderer(this, "Leg4", 16, 14);
leg4.mirror = false;
leg4.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
leg4.setInitialRotationPoint(14.0F, 2.0F, 7.0F);
leg4.setInitialRotationMatrix(new Matrix4f().set(new Quaternion(-5.7054814E-9F, 0.1305262F, 0.9914449F, -4.3337433E-8F)).transpose());
leg4.setTextureSize(128, 128);
parts.put(leg4.boxName, leg4);
body.addChild(leg4);

tailBase = new MCAModelRenderer(this, "TailBase", 0, 13);
tailBase.mirror = false;
tailBase.addBox(0.0F, 0.0F, 0.0F, 2, 3, 4);
tailBase.setInitialRotationPoint(16.0F, 0.0F, 2.0F);
tailBase.setInitialRotationMatrix(new Matrix4f().set(new Quaternion(0.0F, 0.0F, 0.0F, 1.0F)).transpose());
tailBase.setTextureSize(128, 128);
parts.put(tailBase.boxName, tailBase);
body.addChild(tailBase);

tailMid = new MCAModelRenderer(this, "TailMid", 41, 3);
tailMid.mirror = false;
tailMid.addBox(0.0F, 0.0F, 0.0F, 5, 2, 2);
tailMid.setInitialRotationPoint(2.0F, 0.0F, 1.0F);
tailMid.setInitialRotationMatrix(new Matrix4f().set(new Quaternion(0.0F, 0.0F, 0.0F, 1.0F)).transpose());
tailMid.setTextureSize(128, 128);
parts.put(tailMid.boxName, tailMid);
tailBase.addChild(tailMid);

tailTip = new MCAModelRenderer(this, "TailTip", 42, 0);
tailTip.mirror = false;
tailTip.addBox(0.0F, 0.0F, 0.0F, 4, 1, 1);
tailTip.setInitialRotationPoint(5.0F, 0.0F, 0.5F);
tailTip.setInitialRotationMatrix(new Matrix4f().set(new Quaternion(0.0F, 0.0F, 0.0F, 1.0F)).transpose());
tailTip.setTextureSize(128, 128);
parts.put(tailTip.boxName, tailTip);
tailMid.addChild(tailTip);

}

@Override
public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7) 
{
EntityLizard entity = (EntityLizard)par1Entity;

//Render every non-child part
body.render(par7);

AnimationHandler.performAnimationInModel(parts, entity);
}
@Override
public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity par7Entity) {}

public MCAModelRenderer getModelRendererFromName(String name)
{
return parts.get(name) != null ? parts.get(name) : null;
}
}