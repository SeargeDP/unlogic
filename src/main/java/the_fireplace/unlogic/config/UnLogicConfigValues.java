package the_fireplace.unlogic.config;

public class UnLogicConfigValues {
	public static final String DEVKEY_DEFAULT = "";
	public static String DEVKEY;
	public static final String DEVKEY_NAME = "devkey";
	
	public static final Boolean ENABLEMOBS_DEFAULT = true;
	public static Boolean ENABLEMOBS;
	public static final String ENABLEMOBS_NAME = "enableunlogicmobs";

	public static final Boolean ENABLEDEBUG_DEFAULT = false;
	public static Boolean ENABLEDEBUG;
	public static final String ENABLEDEBUG_NAME = "debugmode";
	
	//add Dimension ID if needed
}
