package the_fireplace.unlogic.config;

import java.util.List;

import the_fireplace.unlogic.unbase;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.client.config.GuiConfig;
import cpw.mods.fml.client.config.IConfigElement;

public class UnLogicConfigGui extends GuiConfig {

	public UnLogicConfigGui(GuiScreen parentScreen) {
		super(parentScreen, new ConfigElement(unbase.file.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), "unlogic", false,
				false, GuiConfig.getAbridgedConfigPath(unbase.file.toString()));
	}

}
